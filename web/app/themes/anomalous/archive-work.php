<?php

$args = array(
  'query' => array(
    'post_type' => 'work',
    'posts_per_page' => 15,
    'tax_query' => array(
      array(
        'taxonomy' => 'portfolio',
        'terms' => array(68),
        'operator' => 'NOT IN'
      )
    )
  )
);

$workArchive = new Controllers\ArchiveWork($args);
$workArchive->show();