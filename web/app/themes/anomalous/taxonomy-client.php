<?php

$args = array(
  'query' => array(
    'post_type' => 'work',
    'posts_per_page' => 18,
    'tax_query' => array(
      array(
        'taxonomy' => 'client',
        'field' => 'slug',
        'terms' => get_query_var('term'),
      )
    )
   )
);

$workArchive = new Controllers\ArchiveWork($args);
$workArchive->show();