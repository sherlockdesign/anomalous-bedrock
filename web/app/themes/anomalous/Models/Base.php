<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  // base can be any kind of post type (page, post etc).
  abstract class Base {
    /**
    * Model args
    * @var object
    */
    protected $args = [];

    /**
    * Stores all model data
    * @var object
    */
    protected $data = array();

    /**
    * Twig worker
    * @var \wptwig\Workers\Twig
    */
    public $timber = null;

    public $template = null;

    /**
     * __construct
     *
     * @param  mixed $args
     *
     * @return void
     */
    public function __construct ($args) {
      $this->args['standalone'] = '';
      $this->args = array_merge($this->args, $args);
      $this->add('args', $this->args);
    }

    /**
     * add
     *
     * @param  mixed $name
     * @param  mixed $value
     *
     * @return void
     */
    public function add( $name, $value ) {
      $this->data[ $name ] = $value;
    }

    /**
     * get
     *
     * @return void
     */
    public function get() {

      // get the main context array
      $context = \Timber::get_context();
      $this->timber->addContext($context);

      wp_reset_query();

      $this->timber->addContext (array(
        'display1' => 'f-headline-xl f-headline-l f-headline-2-m f1 lh-solid ttu fw6 persian-green',
        'display2' => 'f1-xl f2-m f2-l f3 lh-solid din fw7 ttu tracked persian-green',
        'heading1' => 'f2-xl f3-l f3-m f4',
        'heading2' => 'f3-xl f4-l f4-m f5',
        'heading3' => 'f4-xl f5-l f5-m f6',
        'headingStyle' => 'ttu din fw3 tracked',
        'paragraph' => 'f4-xl f4-l f5',
        'button' => 'db dib-l mb2 ph5 pv3 f3 montserrat lh-solid tracked fw3 ttu no-underline tc',
        'global' => array(
          'page_for_posts' => get_permalink(get_option('page_for_posts')),
          'year' => date('Y'),
          'body_class' => implode(' ', get_body_class()),
          'menus' => array(
            'primary' => new \TimberMenu('primary'),
            'secondary' => new \TimberMenu('secondary'),
            'workCategories' => new \TimberMenu('work-categories'),
            'services' => new \TimberMenu('services'),
            'footer' => new \TimberMenu('footer'),
          ),
          'anomalous_contacts' => CustomFields::roughhands_get_option('anomalous-visuals-contacts', 'all'),
          'company_info' => CustomFields::roughhands_get_option('company-info', 'all'),
          'map' => CustomFields::roughhands_get_option('map', 'all'),
          'social_media' => CustomFields::roughhands_get_option('social-media', '_anomalous_meta_socialMediaLinks'),
          'copyright' => '&copy; ' . date('Y') . ' ' . CustomFields::roughhands_get_option('settings-company', CustomFields::$prefix . 'company_name'),
          'footerLogos' => CustomFields::roughhands_get_option('settings-company', CustomFields::$prefix . 'footer_logos'), // WordPress
        )
      ));

      // put timber context in the $data variable
      $this->context = $this->timber->context;

      // force array for twig
      return $this->context;
    }

    /**
     * context
     *
     * @return void
     */
    public function context () {
      // put timber context in the $data variable
      $this->data->context = $this->timber->context;

      // force array for twig
      return $this->data->content;
    }

    /**
     * forceArray
     * 
     * The WP widget class does have trouble see Models/Widget->get()
     * so force the data to be an array.
     *
     * @param  mixed $data
     *
     * @return void
     */
    protected function forceArray ($data) {
      return json_decode(json_encode($data), true);
    }

    /**
     * addSidebarsToContext
     *
     * @param  mixed $sidebars
     *
     * @return void
     */
    public function addSidebarsToContext ($sidebars) {
      foreach ($sidebars as $key => $sidebar) {
        $this->timber->addContext( array( $sidebar => \Timber::get_widgets( $sidebar ) ) );
      }
    }

    /**
     * terms
     *
     * @param  mixed $post
     * @param  mixed $args
     * @param  mixed $output
     *
     * @return void
     */
    public function terms ($post, $args = array(), $output = 'names') {
      $terms = wp_get_object_terms ( $post->ID, get_taxonomies ($args, $output));
      $timberTerms = array();

      foreach ($terms as $key => $term) {
        $timberTerms[$key] = new \TimberTerm ($term->term_id );
      }
      return $timberTerms;
    }
    
    /**
     * get_hierachical_terms_by_post
     *
     * @param  mixed $post
     * @param  mixed $tax
     *
     * @return void
     */
    public function get_hierachical_terms_by_post ($post, $tax) {
        $terms = wp_get_post_terms ($post->ID, $tax, array( 'hide_empty' => true));
        $sorted_terms = [];
        $sorted_terms = $this->get_hierachical_terms_by_post_loop ($post, $tax, $terms, $sorted_terms);

        unset( $sorted_terms['children'] );

        return $sorted_terms['sorted_terms'];
    }

    /**
     * get_hierachical_terms_by_post_loop
     *
     * @param  mixed $post
     * @param  mixed $tax
     * @param  mixed $terms
     * @param  mixed $sorted_terms
     *
     * @return void
     */
    public function get_hierachical_terms_by_post_loop ($post, $tax, $terms, $sorted_terms = array()) {
      foreach ($terms as $key => &$term) {
        // get children at current level.
        // $children = wp_get_post_terms($tax, array( 'parent' => $term->term_id, 'hide_empty' => true) );
        $children = wp_get_post_terms( $post->ID, $tax, array( 'parent' => $term->term_id, 'hide_empty' => true) );
        $term->term_permalink = get_term_link( $term->term_id, $tax );

        if ( count($children) > 0 ) {
          // loop through indefinite children (scary).
          $loop = $this->get_hierachical_terms_by_post_loop($post, $tax, $children, $sorted_terms);

          // add returned children to current term.
          $term->children = $loop['children'];
        }
        // Add the current term to final array.
        $sorted_terms[$term->slug] = $term;
      }
      return array('children' => $terms, 'sorted_terms' => $sorted_terms);
    }

    /**
     * get_hierachical_terms
     *
     * @param  mixed $tax
     * @param  mixed $term
     * @param  mixed $args
     * @param  mixed $args_loop
     *
     * @return void
     */
    public function get_hierachical_terms ($tax, $term = null, $args = array(), $args_loop = array()) {
      $sorted_terms = [];
      $args['parent'] = 0;

      if(isset($term)) {
        $args['slug'] = $term;
      }

      $terms = get_terms($tax, $args );

      if(!empty($terms)) {
        $sorted_terms = $this->get_hierachical_terms_loop($tax, $terms, $sorted_terms, $args_loop);
        unset( $sorted_terms['children'] );

        return $sorted_terms['sorted_terms'];
      }
      return null;
    }

    
    /**
     * get_hierachical_terms_loop
     *
     * @param  mixed $tax
     * @param  mixed $terms
     * @param  mixed $sorted_terms
     * @param  mixed $args_loop
     *
     * @return void
     */
    public function get_hierachical_terms_loop ($tax, $terms, $sorted_terms = array(), $args_loop = array( 'hide_empty' => true)) {
      foreach ($terms as $key => &$term) {
        // get children at current level.
        // $children = wp_get_post_terms($tax, array( 'parent' => $term->term_id, 'hide_empty' => true) );

        // changed from 'child_of' to 'parent' so that hierarchy is actually a hierarchy
        $args_loop['parent'] = $term->term_id;
        $children = get_terms( $tax, $args_loop, $args_loop  );
        $term->term_permalink = get_term_link( $term->term_id, $tax);

        if( count($children) > 0 ) {
          // loop through indefinite children (scary).
          $loop = $this->get_hierachical_terms_loop($tax, $children, $sorted_terms);

          // add returned children to current term.
          $term->children = $loop['children'];
        }
        // Add the current term to final array.
        $sorted_terms[$term->slug] = $term;
      }

      return array('children' => $terms, 'sorted_terms' => $sorted_terms);
    }

    /**
     * addToObj
     *
     * @param  mixed $objs
     * @param  mixed $propertyName
     * @param  mixed $propertyValue
     *
     * @return void
     */
    public function addToObj ($objs = array(), $propertyName, $propertyValue) {
      if (is_array ($objs)) {
        foreach ($objs as $key => &$obj) {
          if (is_object ($obj)) {
            $obj->{$propertyName} = $propertyValue($obj);
          } else {
            return null;
          }
        }
      } else {
        if (is_object($objs)) {
          $objs->{$propertyName} = $propertyValue($objs);
        } else {
          return null;
        }
      }
      return $objs;
    }

    /**
     * query
     *
     * @param  mixed $args
     *
     * @return void
     */
    public function query ($args) {
      if (isset($args['query'])) {
        $query = $args['query'];
      }

      // var_dump($args['query']);
      
      query_posts( $query );
      return \Timber::get_posts( $query );
    }

    /**
     * createVideoURLs
     *
     * @param  mixed $videoURL
     *
     * @return void
     */
    public function createVideoURLs($videoURL) {
      if( !empty( $videoURL ) ) {
        $videoURL = substr($videoURL, 0, -3);
        $sources = [];
        $sources['3gp'] = $videoURL . '3gp';
        $sources['mp4'] = $videoURL . 'mp4';
        $sources['ogv'] = $videoURL . 'ogv';
        $sources['webm'] = $videoURL . 'webm';

        return $sources;
      }
      return null;
    }
  }
