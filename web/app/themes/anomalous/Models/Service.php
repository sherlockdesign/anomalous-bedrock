<?php

namespace Models;

use ContentTypes\CustomFields as CustomFields;

class Service extends Single {
  
  /**
   * __construct
   *
   * @param  mixed $args
   *
   * @return void
   */
  public function __construct($args) {
      parent::__construct($args);
  }

  /**
   * get
   *
   * @return void
   */
  public function get() {

		$context = array(
			'hero' => $this->getHero(),
			'testimonial' => $this->getTestimonial(),
			'mainContent' => $this->getMainContent(),
			'teamMember' => $this->getTeamMember(),
			'clientCarousel' => $this->getClientCarousel(),
			'featuredPosts' => $this->getFeaturedPosts(),
		);

		if(isset($this->post->custom['_anomalous_meta_heroBackgroundCarousel'])) {
			array_walk($this->post->custom['_anomalous_meta_heroBackgroundCarousel'], function(&$image, $key) {
				$image = Single::getProgressiveImageLoadingObj(
					$key
				);
			}); 
		}

		$this->post->recentPosts = $this->getRecentPosts();

		$this->timber->addContext($context);

		return parent::get();
	}

	private function getRecentPosts () {
		$params = array( 'query' => array(
			'post_type' => 'work',
			'posts_per_page' => 4,
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'work-categories',
					'field' => 'slug',
					'terms' => $this->post->slug
				)
			)
		));
		$recentPosts = new \Controllers\Archive($params);
		return $recentPosts->returnData('archive')['posts'];
	}

	/**
	 * getHero
	 *
	 * @return void
	 */
	private function getHero() {
		return array(
			'backgroundImage' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'heroBackgroundImage_id', true)
			),
			'heroParagraph1' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'heroParagraph1', true),
			'heroParagraph2' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'heroParagraph2', true),
		);
	}

	/**
	 * getTestimonial
	 *
	 * @return void
	 */
	private function getTestimonial() {
		if(isset($this->post->custom['_anomalous_meta_testimonialParalaxImage'])) {
			$this->post->custom['_anomalous_meta_testimonialParalaxImage'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_testimonialParalaxImage_id']
			);  
		}
		if(isset($this->post->custom['_anomalous_meta_textBlockImage1'])) {
			$this->post->custom['_anomalous_meta_textBlockImage1'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_textBlockImage1_id']
			);  
		}
		if(isset($this->post->custom['_anomalous_meta_textBlockImage2'])) {
			$this->post->custom['_anomalous_meta_textBlockImage2'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_textBlockImage2_id']
			);  
		}
		if(isset($this->post->custom['_anomalous_meta_servicesParalaxImage'])) {
			$this->post->custom['_anomalous_meta_servicesParalaxImage'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_servicesParalaxImage_id']
			);  
		}
		if(isset($this->post->custom['_anomalous_meta_heroBackgroundImage'])) {
			$this->post->custom['_anomalous_meta_heroBackgroundImage'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_heroBackgroundImage_id']
			);  
		}
		return array(
			'testimonialParalaxImage' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialParalaxImage_id', true)
			),
			'testimonialContent' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialContent', true),
			'testimonialAuthor' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialAuthor', true),
		);
	}
	
	/**
	 * getMainContent
	 *
	 * @return void
	 */
	private function getMainContent() {
		return array(
			'textBlock1' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'textBlock1', true),
			'textBlockImage1' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'textBlockImage1_id', true)
			),
			'paralaxImage' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'paralaxImage_id', true)
			),
			'textBlock2' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'textBlock2', true),
			'textBlockImage2' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'textBlockImage2_id', true)
			),
		);
	}
}