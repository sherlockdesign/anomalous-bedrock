<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class StyleGuide extends Page {
    /**
     * __construct
     *
     * @param  mixed $args
     *
     * @return void
     */
    public function __construct($args) {
      parent::__construct($args);
    }

    /**
     * get
     *
     * @return void
     */
    public function get () {

      /** 
       * Raw text and any other content that does not need to be further manipulated
       * does not need to be fetched with a 'get_post_meta' call. It is already stored 
       * under the property 'custom' of the post.
      */

      // die(var_dump($this->post));

      // Add it all to the timber context
      $this->timber->addContext(array(
        'teamMember' => $this->getTeamMember(),
      ));

      $this->gallery();

      // die(var_dump($this->getTeamMember()));

      // call the parent function
      return parent::get();
    }

    private function getTeamMember() {
      $id = get_post_meta ($this->post->ID, CustomFields::$prefix . 'meetTeamMember', true);
      if (!empty($id)) {
        $teamMember = new \Controllers\Single(array(
          'id' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'meetTeamMember', true)
        ));
        return $teamMember->returnData('post');
      }
    }

    private function gallery() {
      array_walk($this->post->custom['_anomalous_meta_gallery'], function(&$image, $key) {
        $image = Single::getProgressiveImageLoadingObj(
          $key
        );
      });
      // die(var_dump($this->post->custom['_anomalous_meta_gallery']));
    }
  }
