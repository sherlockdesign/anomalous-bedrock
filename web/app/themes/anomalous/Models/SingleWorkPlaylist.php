<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class SingleWorkPlaylist extends SingleWork {

    /**
    *   @method __construct
    *   @return get
    **/
    public function __construct ($args) {
      parent::__construct($args);
    }

    /**
    *   @method get
    *   @return parent::get()
    *
    *
    **/
    public function get() {

      // $context = array(
      //   'contentVideo' => $this->getContentVideo(),
      //   'testimonialVideo' => $this->getTestimonialVideo(),
      // );

      // $this->getVideoGallery();

      // if (isset($_GET['dump'])) {
      //   die(var_dump($context));
      // }


      // $this->timber->addContext($context);

      return parent::get();
    }


    private function getContentVideo() {
      return array(
        'url' => get_post_meta( $this->post->ID, CustomFields::$prefix . 'contentVideovideoURL', true),
        'fallbackImage' => Single::getProgressiveImageLoadingObj(
          get_post_meta ($this->post->ID, CustomFields::$prefix . 'contentVideoFallbackImage_id', true)
        ),
      );
    }

    private function getTestimonialVideo() {
      return array(
        'url' => get_post_meta( $this->post->ID,CustomFields::$prefix . 'testimonialVideovideoURL', true),
        'fallbackImage' => Single::getProgressiveImageLoadingObj(
          get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialVideoFallbackImage_id', true)
        ),
      );
    }

    // array (size=2)
    //   0 => 
    //     array (size=3)
    //       'url' => string 'https://player.vimeo.com/video/168069186' (length=40)
    //       'thumbnail_id' => int 1418
    //       'images' => 
    //         object(stdClass)[432]
    //           public 'mobile' => string 'https://develop2.anomalous.com/app/uploads/2019/05/Phillip_B-5DsR0160©ivan-weiss-800x533.jpg' (length=93)
    //   1 => 

    /**
     * getVideoGallery
     *
     * @return void
     */
    private function getVideoGallery() {
      if(isset($this->post->custom['_anomalous_meta_videoGallery'])) {
        
        $videoGallery = $this->post->custom['_anomalous_meta_videoGallery'];

        array_walk($videoGallery, function(&$field, $key) {
            unset($field['thumbnail']);
            $field['images'] = Single::getProgressiveImageLoadingObj(
              $field['thumbnail_id']
          );
        }); 

        $this->post->custom['_anomalous_meta_videoGallery'] = $videoGallery;

      }
    }

  }

  