<?php

namespace Models;

use ContentTypes\CustomFields as CustomFields;

class SingleSpace extends Single {
  
  /**
   * __construct
   *
   * @param  mixed $args
   *
   * @return void
   */
  public function __construct($args) {
      parent::__construct($args);
  }

  /**
   * get
   *
   * @return void
   */
  public function get() {		

		$context = array(
			// 'heroImage' => $this->getHero(),
			// 'facilities' => $this->getFacilities(),
			// 'gallery' => $,
		);
		
		$this->getGallery();

		if (isset($_GET['dump'])) {
			die(var_dump($this->post));
		}

		if(isset($this->post->custom['_anomalous_meta_backgroundImage'])) {
			$this->post->custom['_anomalous_meta_backgroundImage'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_backgroundImage_id']
			);  
		}

		if(isset($this->post->custom['_anomalous_meta_facilities3dUrlImage'])) {
			$this->post->custom['_anomalous_meta_facilities3dUrlImage'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_facilities3dUrlImage_id']
			);  
		}

		$this->timber->addContext($context);

		return parent::get();
	}

	/**
	 * getTestimonialImages
	 *
	 * @return void
	 */
	private function getHero() {
		return get_post_meta ($this->post->ID, CustomFields::$prefix . 'backgroundImage', true);
	}
	
	/**
	 * getFacilities
	 *
	 * @return void
	 */
	private function getFacilities() {
		return array(
			'facilitiesContent' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'facilitiesContent', true),
			'facilitiesImage' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'facilitiesImage_id', true)
			),
			'facilities3dUrl' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'facilities3dUrl', true),
			'facilities3dUrlImage' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'facilities3dUrlImage_id', true)
			),
		);
	}

	/**
	 * getGallery
	 *
	 * @return void
	 */
	private function getGallery() {
		if(isset($this->post->custom['_anomalous_meta_gallery'])) {
			array_walk($this->post->custom['_anomalous_meta_gallery'], function(&$image, $key) {
				$image = Single::getProgressiveImageLoadingObj(
					$key
				);
			}); 
		}
	}
}