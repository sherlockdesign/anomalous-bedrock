<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class SinglePost extends Single {

    /**
    *   @method __construct
    *   @return get
    **/
    public function __construct ($args) {
      parent::__construct($args);
    }

    /**
    *   @method get
    *   @return parent::get()
    *
    *
    **/
    public function get() {

      // $post->relatedPosts
      $this->addRelatedPosts();

      // die(var_dump($this->post));

      return parent::get();
    }
  }
