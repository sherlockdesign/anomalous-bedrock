<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class RelatedPosts extends Base {

    // public $relatedPosts = array();

    // public $relatedPostsNotIn = array();

    /**
    *   @method __construct
    *   @return get
    **/
    public function __construct ($args) {
      parent::__construct($args);
    }

    /**
    *   @method get
    *   @return parent::get()
    *
    *
    **/
    public function get() {
      
      $this->posts = $this->query($this->args);

      array_walk($this->posts, function(&$post, $key) {
        $post->featured_image = get_the_post_thumbnail(
          $post->ID, 
          'work-archive-16x9', 
          array('class' => 'h-auto w-100 aspect-ratio--object z-1')
        );
        $post->featuredImage = new \stdClass;
        $post->featuredImage->PLImage = Single::getProgressiveImageLoadingObj(
          get_post_thumbnail_id($post->ID),
          'work-archive-16x9', 
          'h-auto w-100 aspect-ratio--object z-1'
        );
      });

      $context = array(
        'archive' => array(
          'posts' => $this->posts,
        ),
      );

			$this->timber->addContext($context);

      return parent::get();
    }
  }
