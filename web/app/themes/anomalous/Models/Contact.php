<?php

namespace Models;

use ContentTypes\CustomFields as CustomFields;

class Contact extends Single {
  /**
   * __construct
   *
   * @param  mixed $args
   *
   * @return void
   */
  public function __construct($args) {
    parent::__construct($args);
    $this->featuredImageSize = 'gallery_image--desktop';
  }

  /**
   * get
   *
   * @return void
   */
  public function get() {
    $jobs = new \Controllers\Page(array(
      'slug' => 'jobs',
      'standalone' => true,
    ));

    $this->timber->addContext(array(
      'jobsHTML' => $jobs->compile(),
    ));

    return parent::get();
  }
}