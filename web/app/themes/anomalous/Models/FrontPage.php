<?php

namespace Models;

use ContentTypes\CustomFields as CustomFields;

class Frontpage extends Single {
  
  /**
   * __construct
   *
   * @param  mixed $args
   *
   * @return void
   */
  public function __construct($args) {
      parent::__construct($args);
  }

  /**
   * get
   *
   * @return void
   */
  public function get() {

		// $frontpageSquaresArray = get_post_meta($this->post->ID, CustomFields::$prefix . 'frontpageSquares', true);
		// $frontpageSquares = $this->getFallbackImages($frontpageSquaresArray);
	
		// $context = array(
		// 	'frontpageSquares' => $frontpageSquares
		// );

		// $this->timber->addContext($context);

    return parent::get();
	}

	/**
	 * getFallbackImages
	 *
	 * @param  mixed $array
	 *
	 * @return void
	 */
	public function getFallbackImages($array) {
		if(is_array($array)) {
			foreach ($array as $key => &$item) {
				if(isset($item['post'])) {
					$post = new \Controllers\Single(array(
						'id' => $item['post']
					));
					$postObj = $post->returnData('post');
					$item['post'] = array( 
						'title' => $postObj->post_title,
						'link' => $postObj->link,
						'content' => $postObj->content
					);
				}
				if(isset($item['backgroundImage_id'])) {
					$item['backgroundImage'] = Single::getProgressiveImageLoadingObj(
						$item['backgroundImage_id']
					);
				}
			}
		}
		return $array;
	}
}
