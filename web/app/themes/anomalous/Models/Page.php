<?php

  namespace Models;

  // use ContentTypes\CustomFields as CustomFields;

  class Page extends Single {

    /**
     * __construct
     *
     * @param  mixed $args
     *
     * @return void
     */
    public function __construct ($args) {
      parent::__construct ($args);
    }

    /**
     * get
     *
     * @return void
     */
    public function get () {
      return parent::get();
    }
  }
