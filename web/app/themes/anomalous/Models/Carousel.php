<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class Carousel extends Page {
    /**
     * __construct
     *
     * @param  mixed $args
     *
     * @return void
     */
    public function __construct($args) {
      parent::__construct( $args );
    }

    /**
     * get
     *
     * @return void
     */
    public function get() {
      // Get the slides
      $proudSlider = get_post_meta( $this->post->ID, CustomFields::$prefix . 'proudAtWhatWeDoSlider', true );

      // Add it all to the timber context
      $this->timber->addContext( array(
        'proudAtWhatWeDoSliderBlocks' => $proudSlider,
      ) );

      // Get the parent context (page, single (post thumbnails etc..))
      return parent::get();
    }
  }
