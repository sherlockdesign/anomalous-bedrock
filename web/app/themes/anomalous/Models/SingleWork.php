<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class SingleWork extends Single  {

    /**
    *   @method __construct
    *   @return get
    **/
    public function __construct ($args) {
      parent::__construct($args);
    }

    /**
    *   @method get
    *   @return parent::get()
    *
    *
    **/
    public function get() {

      $context = array(
        'mainVideo' => $this->getMainVideo(),
        'testimonial' => $this->getTestimonial(),
      );

      if (isset($_GET['dump'])) {
        die(var_dump($context));
      }

      $this->timber->addContext($context);

      $this->gallery();
      $this->parallax();

      $this->post->custom['testimonialVideoImage'] = Single::getProgressiveImageLoadingObj( 
        get_post_meta( $this->post->ID,'testimonialVideoImage_id', true)
      );

      $this->post->custom['contentVideoImage'] = Single::getProgressiveImageLoadingObj( 
        get_post_meta( $this->post->ID,'contentVideoImage_id', true)
      );

      // die(var_dump($this->post->custom));


      return parent::get();
    }

    private function getTestimonial() {
      return array(
        'testimonialContent' => get_post_meta( $this->post->ID,CustomFields::$prefix . 'testimonialContent', true),
        'testimonialAuthor' => get_post_meta( $this->post->ID,CustomFields::$prefix . 'testimonialAuthor', true),
      );
    }

    private function getMainVideo() {
      return array(
        'videoURL' => get_post_meta( $this->post->ID,CustomFields::$prefix . 'videoURL', true),
        'videoFallbackImage' => Single::getProgressiveImageLoadingObj(
          get_post_meta ($this->post->ID, CustomFields::$prefix . 'videoFallbackImage_id', true)
        ),
      );
    }

    /**
     * getGallery
     *
     * @return void
     */
    protected function getGallery($fieldID) {
      $gallery = get_post_meta ($this->post->ID, CustomFields::$prefix . $fieldID, true);
      if(is_array($gallery)) {
        array_walk($gallery, function(&$item, $key) {
          $item = Single::getProgressiveImageLoadingObj($key);
        });
        return $gallery;
      }
      return false;
    }

    private function gallery() {
      if(isset($this->post->custom['_anomalous_meta_gallery'])) {
        array_walk($this->post->custom['_anomalous_meta_gallery'], function(&$image, $key) {
          $image = Single::getProgressiveImageLoadingObj(
            $key
          );
        }); 
      }
    }

    private function parallax() {
      if(isset($this->post->custom['_anomalous_meta_testimonialParalaxImage'])) {
        $this->post->custom['_anomalous_meta_testimonialParalaxImage'] = Single::getProgressiveImageLoadingObj(
          $this->post->custom['_anomalous_meta_testimonialParalaxImage_id']
        );  
      }
      if(isset($this->post->custom['_anomalous_meta_paralaxImage'])) {
        $this->post->custom['_anomalous_meta_paralaxImage'] = Single::getProgressiveImageLoadingObj(
          $this->post->custom['_anomalous_meta_paralaxImage_id']
        );  
      }
    }


  }

  