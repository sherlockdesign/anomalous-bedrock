<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class Single extends Base {

    public $sidebars = array();

    public $relatedPosts = array();

    public $relatedPostsNotIn = array();

    /**
    *   @method __construct
    *   @return get
    **/
    public function __construct ($args) {
      parent::__construct($args);

      // Set Sidebars and merge
      $this->sidebars = array(
        'sidebar',
        'sidebar__footer',
      );

      if (isset($this->args['sidebars'])) {
        array_merge ($this->sidebars, $this->args['sidebars']);
      }

      // Accept slug and id as parameters for getting the Post Obj
      if (isset ($this->args['slug'])) {
        $this->post = new \TimberPost ($this->args['slug']);
      } else if( isset($this->args['id']) ) {
        $this->post = new \TimberPost ($this->args['id']);
      } else {
        $this->post = new \TimberPost ();
      }

      $this->featuredImageID =  get_post_thumbnail_id($this->post->ID);
      $this->featuredImageMetaData =  wp_get_attachment_metadata($this->featuredImageID);

      $this->addToObj ($this->post, 'subtitle', function ($post) {
        return get_post_meta( $post->ID, CustomFields::$prefix . 'page_subtitle', true );
      });

			// Retrieve Array of Taxonomy names
			$this->post->taxonomies = get_post_taxonomies($this->post);

			$this->post->taxonomyObjs = [];

			if (!empty($this->post->taxonomies)) {
				foreach ($this->post->taxonomies as $key => $taxonomy) {
					// Retrieve Taxonomy object
					$this->post->taxonomyObjs[$taxonomy] = get_taxonomy($taxonomy);

					// Retrieve Array of Term Objects attached Post that are not emmpty.
					$this->post->taxonomyObjs[$taxonomy]->termObjs = wp_get_post_terms( 
						$this->post->ID, $taxonomy, 
						array('hide_empty' => true) 
					);

					// Get hierarchically sorted Array of Term obj
					$this->post->taxonomyObjs[$taxonomy]->hierarchicalTermObjs = $this->get_hierachical_terms_by_post(
						$this->post, 
						$taxonomy
					);

					// Create terms Array and fill with the term slugs.
					$this->post->taxonomyObjs[$taxonomy]->terms = [];
					foreach ($this->post->taxonomyObjs[$taxonomy]->termObjs as $key => $term) {
						$this->post->taxonomyObjs[$taxonomy]->terms[$key] = $term->term_id;
					}
				}
			}
    }

    public function getFallbackImages($array) {
      if(is_array($array)) {
        foreach ($array as $key => &$item) {
          if(isset($item['post'])) {

            $postObj = new \TimberPost ($item['post']);
            
            $item['post'] = array( 
              'title' => $postObj->post_title,
              'link' => $postObj->link,
              'content' => $postObj->content
            );
          }
          if(isset($item['backgroundImage_id'])) {
            $item['backgroundImage'] = Single::getProgressiveImageLoadingObj(
              $item['backgroundImage_id']
            );
          }
        }
      }
      return $array;
    }

    /**
    *   @method get
    *   @return parent::get()
    *
    *
    **/
    public function get() {
    
      $this->post->featuredImage = self::getProgressiveImageLoadingObj(
        $this->featuredImageID
      );

      // Add sidebars to context
      $this->addSidebarsToContext($this->sidebars);

      // get post type obj
      $this->post->post_type = get_post_type($this->post);

      $frontpageSquaresArray = get_post_meta(5, CustomFields::$prefix . 'frontpageSquares', true);
      $frontpageSquares = $this->getFallbackImages($frontpageSquaresArray);

      $this->timber->addContext (array(
        'post' => $this->post,
        'standalone' => $this->data['args']['standalone'],
        'frontpageSquares' => $frontpageSquares,
        'frontpagePost' => new \TimberPost (5),
      ));

      return parent::get();
    }

    /**
     * addRelatedPosts
     * 
     * This function itterates over terms creating a query that will query the post_status
     * for all terms and then drop of a term until it finds at least 3.
     *
     * @return void
     */
    public function addRelatedPosts() {
      // Base args
      $mainQuery = array( 'query' => array(
        'post__not_in' => array($this->post->ID),
        'post_type' => $this->post->post_type,
        'orderby' => 'relevance',
        'posts_per_page' => 3,
        'post_status' => 'publish',
      ));
      // die(var_dump($this->post->post_type));
      if ($this->post->post_type === 'post') {
        foreach ($this->post->taxonomyObjs['category']->terms as $key => $term) {
          $this->relatedPostsArgs[$key] = $mainQuery;
          $incrementTerms[] = $term;
          $this->relatedPostsArgs[$key]['category__and'][] = $incrementTerms;
        }
        $this->relatedPostsArgs = array_reverse($this->relatedPostsArgs);
      } else if ($this->post->post_type === 'work') {
        foreach ($this->post->taxonomyObjs['type']->terms as $key => $term) {
          $this->relatedPostsArgs[$key]['query']['tax_query'] = [];
          $this->relatedPostsArgs[$key] = $mainQuery;
          $incrementTerms[] = $term;
          $this->relatedPostsArgs[$key]['query']['tax_query'][] = array(
            'taxonomy' => 'type',
            'field' => 'term_id',
            'terms' => $incrementTerms,
            'operator' => 'AND'
          );
        }
        $this->relatedPostsArgs = array_reverse($this->relatedPostsArgs);
      } else {
        return false;
      }

      // die(var_dump($this->relatedPostsArgs[0]));

      // Run the query and add to the Post Obj
      $this->addToObj($this->post, 'relatedPosts', function( $postObj ) {

        // Do we have multiple queries?
        if( empty($this->relatedPostsArgs['query']) ) {

          // Loop through each query
          foreach ($this->relatedPostsArgs as $key => $relatedPostsArgs) {

            // If no post__not_in are set, define it now
            if (!empty($this->relatedPostsNotIn)) {
              $this->relatedPostsArgs[$key]['query']['post__not_in'] = array_merge(
                $this->relatedPostsNotIn,
                $this->relatedPostsArgs[$key]['query']['post__not_in']
              );
            }

            // Run the query of this key and return the posts
            $relatedPosts = new \Controllers\Archive($this->relatedPostsArgs[$key]);
            $archive = $relatedPosts->returnData('archive');

            // if this is the first loop, define RelatedPosts prop
            if ($key === 0) {
              $this->RelatedPosts = $archive['posts'];

            // otherwise, merge the relatedPosts
            } else {
              $this->RelatedPosts = array_merge($this->RelatedPosts, $archive['posts']);
            }

            // if we now have at least 3 posts
            // if (count($this->RelatedPosts) >= 3) {
            if (true) {
              // remove all posts after index 2 (no more than 3)
              $offsetKey =2 ; //<--- The offset you need to grab
              $n = array_keys($this->RelatedPosts); //<---- Grab all the keys of your actual array and put in another array
              $count = array_search($offsetKey,$n); //<--- Returns the position of the offset from this array using search
              $this->RelatedPosts = array_slice($this->RelatedPosts, 0, $count + 1, true);
              
              return $this->RelatedPosts;
            } else {

              // get the ids of current query posts to add them to the 'not in' param
              $this->relatedPostsNotIn = wp_list_pluck($this->RelatedPosts, 'ID');
            }
          }
        } else {
          $relatedPosts = new \Controllers\RelatedPosts($this->relatedPostsArgs);
          return $relatedPosts->returnData('archive');
        }
        return null;
      });
    }

    public static function getProgressiveImageLoadingObj($featuredImageID) {
      $featuredImage = new \stdClass;
      $featuredImage->mobile = wp_get_attachment_image_url(
        $featuredImageID, 
        'mobile',
        null
      );
      $featuredImage->mobile1x1 = wp_get_attachment_image_url(
        $featuredImageID, 
        'mobile-1x1',
        null
      );
      $featuredImage->mobilex2 = wp_get_attachment_image_url(
        $featuredImageID, 
        'mobile-x2',
        null
      );
      $featuredImage->tabletx2 = wp_get_attachment_image_url(
        $featuredImageID, 
        'tablet',
        null
      );
      $featuredImage->tablet1x1 = wp_get_attachment_image_url(
        $featuredImageID, 
        'tablet-1x1',
        null
      );
      $featuredImage->tabletx2 = wp_get_attachment_image_url(
        $featuredImageID, 
        'tablet-x2',
        null
      );
      $featuredImage->desktop = wp_get_attachment_image_url(
        $featuredImageID, 
        'desktop',
        null
      );
      $featuredImage->desktop1x1 = wp_get_attachment_image_url(
        $featuredImageID, 
        'desktop-1x1',
        null
      );
      $featuredImage->desktopx2 = wp_get_attachment_image_url(
        $featuredImageID, 
        'desktop-x2',
        null
      );
      $featuredImage->tiny = wp_get_attachment_image_url(
        $featuredImageID, 
        'tiny'
      );
      $featuredImage->desktop3x4 = wp_get_attachment_image_url(
        $featuredImageID, 
        'desktop-3x4'
      );
      $featuredImage->mobile3x4 = wp_get_attachment_image_url(
        $featuredImageID, 
        'mobile-3x4'
      );
      return $featuredImage;
    }

    /**
     * getClientCarousel
     *
     * @return void
     */
    public function getClientCarousel() {
      $gallery = get_post_meta ($this->post->ID, 'clientsCarousel', true);
      if(is_array($gallery)) {	
        array_walk($gallery, function(&$item, $key) {
          $item['logo'] = wp_get_attachment_image_url(
            $item['logo_id'], 
            'medium',
            null
          );
        }); 
      }
      return $gallery;
    }
    
    /**
     * getFeaturedPosts
     *
     * @return void
     */
    public function getFeaturedPosts() {
      $featuredPostsIDs = get_post_meta($this->post->ID, 'featuredPosts', true);
      $featuredPosts = [];
      if (is_array($featuredPostsIDs)) {
        if (count($featuredPostsIDs) == 4) {
          foreach ($featuredPostsIDs as $key => $value) {
            $single = new \Controllers\SingleWork(array('id' => $value['post']));
            $featuredPosts[] = $single->returnData('post');
          }
          return $featuredPosts;
        }
      }
      return false;
    }


    /**
     * getTeamMember
     *
     * @return void
     */
    public function getTeamMember() {
      $id = get_post_meta ($this->post->ID, CustomFields::$prefix . 'meetTeamMember', true);
      if (!empty($id)) {
        $teamMember = new \Controllers\Single(array(
          'id' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'meetTeamMember', true)
        ));
        return $teamMember->returnData('post');
      }
    }
  }
