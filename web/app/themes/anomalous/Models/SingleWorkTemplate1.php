<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class SingleWorkTemplate1 extends SingleWork {

    /**
    *   @method __construct
    *   @return get
    **/
    public function __construct ($args) {
      parent::__construct($args);
    }

    /**
    *   @method get
    *   @return parent::get()
    *
    *
    **/
    public function get() {

      $context = array(
        'contentVideo' => $this->getContentVideo(),
        'testimonialVideo' => $this->getTestimonialVideo(),
      );

      if (isset($_GET['dump'])) {
        die(var_dump($context));
      }

      $this->timber->addContext($context);

      return parent::get();
    }


    private function getContentVideo() {
      return array(
        'url' => get_post_meta( $this->post->ID,CustomFields::$prefix . 'contentVideovideoURL', true),
        'fallbackImage' => Single::getProgressiveImageLoadingObj(
          get_post_meta ($this->post->ID, CustomFields::$prefix . 'contentVideoFallbackImage_id', true)
        ),
      );
    }

    private function getTestimonialVideo() {
      return array(
        'url' => get_post_meta( $this->post->ID,CustomFields::$prefix . 'testimonialVideovideoURL', true),
        'fallbackImage' => Single::getProgressiveImageLoadingObj(
          get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialVideoFallbackImage_id', true)
        ),
      );
    }
  }

  