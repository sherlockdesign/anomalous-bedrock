<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class SingleWorkTemplate4 extends SingleWork {

    /**
    *   @method __construct
    *   @return get
    **/
    public function __construct ($args) {
      parent::__construct($args);
    }

    /**
    *   @method get
    *   @return parent::get()
    *
    *
    **/
    public function get() {

      $context = array(
        'paralaxImage' => $this->getParalaxImage(),
        // 'workGallery' => $this->getGallery('workGallery'),
      );

      if (isset($_GET['dump'])) {
        die(var_dump($context));
      }

      $this->progressiveImages();

      $this->timber->addContext($context);

      return parent::get();
    }

    /**
     * getParalaxImage
     *
     * @return void
     */
    private function getParalaxImage() {
      return Single::getProgressiveImageLoadingObj(
        get_post_meta ($this->post->ID, CustomFields::$prefix . 'paralaxImage_id', true)
      );
    }
    
    private function progressiveImages() {
      if(isset($this->post->custom['_anomalous_meta_overlappingImages1'])) {
        $this->post->custom['_anomalous_meta_overlappingImages1'] = Single::getProgressiveImageLoadingObj(
          $this->post->custom['_anomalous_meta_overlappingImages1_id']
        ); 
      } 
      if(isset($this->post->custom['_anomalous_meta_overlappingImages2'])) {
        $this->post->custom['_anomalous_meta_overlappingImages2'] = Single::getProgressiveImageLoadingObj(
          $this->post->custom['_anomalous_meta_overlappingImages2_id']
        ); 
      } 
      if(isset($this->post->custom['_anomalous_meta_overlappingImages3'])) {
        $this->post->custom['_anomalous_meta_overlappingImages3'] = Single::getProgressiveImageLoadingObj(
          $this->post->custom['_anomalous_meta_overlappingImages3_id']
        ); 
      } 
      if(isset($this->post->custom['_anomalous_meta_overlappingImages4'])) {
        $this->post->custom['_anomalous_meta_overlappingImages4'] = Single::getProgressiveImageLoadingObj(
          $this->post->custom['_anomalous_meta_overlappingImages4_id']
        ); 
      } 
    }
  }

  