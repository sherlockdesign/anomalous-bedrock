<?php

namespace Models;

use ContentTypes\CustomFields as CustomFields;

class Space extends Single {
  
  /**
   * __construct
   *
   * @param  mixed $args
   *
   * @return void
   */
  public function __construct($args) {
      parent::__construct($args);
  }

  /**
   * get
   *
   * @return void
   */
  public function get() {

		$spaces = $this->getSpaces();

		array_walk($spaces, function(&$post, $key) {
			if (isset($post->custom['_anomalous_meta_facilities3dUrlImage'])) {
				$post->custom['_anomalous_meta_facilities3dUrlImage'] = Single::getProgressiveImageLoadingObj(
					$post->custom['_anomalous_meta_facilities3dUrlImage_id']
				);
			}
			if(isset($post->custom['_anomalous_meta_gallery'])) {
				array_walk($post->custom['_anomalous_meta_gallery'], function(&$image, $key) {
					$image = Single::getProgressiveImageLoadingObj(
						$key
					);
				}); 
			}
		}); 

		// die(var_dump($this->post->custom));
		
    $context = array(
			'heroImage' => $this->getHero(),
			'testimonial' => $this->getTestimonial(),
			'clientCarousel' => $this->getClientCarousel(),
			'spaces' => $spaces,
			'featuredPosts' => $this->getFeaturedPosts(),
			'teamMember' => $this->getTeamMember(),
		);
		
		if(isset($this->post->custom['_anomalous_meta_heroBackgroundCarousel'])) {
			array_walk($this->post->custom['_anomalous_meta_heroBackgroundCarousel'], function(&$image, $key) {
				$image = Single::getProgressiveImageLoadingObj(
					$key
				);
			}); 
		}

    if (isset($_GET['dump'])) {
			die(var_dump($context));
		}

    $this->timber->addContext($context);
    
    return parent::get();
	}

	/**
	 * getHero
	 *
	 * @return void
	 */
	private function getHero() {
		if(isset($this->post->custom['_anomalous_meta_heroBackgroundImage'])) {
			$this->post->custom['_anomalous_meta_heroBackgroundImage'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_heroBackgroundImage_id']
			);  
		}
		if(isset($this->post->custom['_anomalous_meta_testimonialParalaxImage'])) {
			$this->post->custom['_anomalous_meta_testimonialParalaxImage'] = Single::getProgressiveImageLoadingObj(
				$this->post->custom['_anomalous_meta_testimonialParalaxImage_id']
			);  
		}
		return array(
			'backgroundImage' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'heroBackgroundImage_id', true)
			),
			'heroParagraph1' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'heroParagraph1', true),
			'heroParagraph2' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'heroParagraph2', true),
		);
	}

	/**
	 * getTestimonial
	 *
	 * @return void
	 */
	private function getTestimonial() {
		return array(
			'testimonialParalaxImage' => Single::getProgressiveImageLoadingObj(
				get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialParalaxImage_id', true)
			),
			'testimonialContent' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialContent', true),
			'testimonialAuthor' => get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialAuthor', true),
		);
	}

	public function getSpaces () {
		$params = array( 'query' => array(
			'post_type' => 'spaces',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC'
		));
		$spaces = new \Controllers\Archive($params);
		return $spaces->returnData('archive')['posts'];
	}
	
}
