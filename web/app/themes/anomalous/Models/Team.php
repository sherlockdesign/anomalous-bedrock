<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class Team extends Single {
    /**
     * __construct
     *
     * @param  mixed $args
     *
     * @return void
     */
    public function __construct($args) {;

      // add_filter( 'posts_orderby' , array('Includes\Utils', 'posts_orderby_lastname') );

      parent::__construct($args);
    }

    /**
     * get
     *
     * @return void
     */
    public function get() {
      $context = array(
        'teamMembers' => $this->getTeamMembers(),
        'directors' => $this->getDirectors(),
      );
  
      if (isset($_GET['dump'])) {
        die(var_dump($context));
      }
  
      $this->timber->addContext($context);

      return parent::get();
    }

    private function getTeamMembers() {
      $params = array( 'query' => array(
        'post_type' => 'team-member',
        'posts_per_page' => -1,
        'tax_query' => array(
          array( 
            'taxonomy'  => 'position',
            'field'     => 'slug',
            'terms'     => 'director',
            'operator'  => 'NOT IN'
          )
        )
      ));
      $teamMembers = new \Controllers\Archive($params);
      return $teamMembers->returnData('archive')['posts'];
    }

    private function getDirectors() {
      $params = array( 'query' => array(
        'post_type' => 'team-member',
        'posts_per_page' => -1,
        'tax_query' => array(
          array( 
            'taxonomy'  => 'position',
            'field'     => 'slug',
            'terms'     => 'director',
            'operator'  => 'IN'
          )
        )
      ));
      $teamMembers = new \Controllers\Archive($params);
      return $teamMembers->returnData('archive')['posts'];
    }

    private function addToArchive($posts) {
      remove_filter( 'posts_orderby' , array('Utils', 'posts_orderby_lastname') );
  
      // Add position term
      array_walk($posts, function(&$post, $key) {
        $post->position =  wp_get_object_terms($post->ID, 'position');
      });
    
      return $posts;
    }
  }