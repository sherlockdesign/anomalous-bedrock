<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class Archive extends Page {
  
    public $args = array();

    public $post = null;

    public $posts = null;
    
    /**
     * __construct
     *
     * @param  mixed $args
     *
     * @return void
     */
    public function __construct ($args) {
      add_filter('get_the_archive_title' , array(__CLASS__, 'get_the_archive_title'));
      $args['query']['post_status'] = 'publish';

      parent::__construct($args);
    }

    /**
     * addToArchive
     *
     * @param  mixed $posts
     *
     * @return void
     */
    public function addToArchive($posts) {
      return null;
    }
    
    /**
     * get
     *
     * @return void
     */
    public function get() {
      $this->posts = $this->query($this->args);

      // Get the custom post thumbnail <img>
      array_walk($this->posts, function(&$post, $key) {
        $post->featuredImage = Single::getProgressiveImageLoadingObj(
          get_post_thumbnail_id($post->ID)
        );
        
        $post->subtitle = get_post_meta( $post->ID, CustomFields::$prefix . 'page_subtitle', true );
      });

      // Check if addToArchive func is defined
      // If so, call it.
      $posts = $this->addToArchive($this->posts);
      if ($posts !== null) {
        $this->posts = $posts;
      }
      
      // Add the data we need to asyncrhonously load more posts.
      $this->addMorePostData();

      $context = array(
        'archive' => array(
          'title' => get_the_archive_title(),
          'posts' => $this->posts,
          'pagination' => \Timber::get_pagination(),
          'more' => $this->archive_more,
          'found' => $this->archive_found,
          'term' => get_query_var('term'),
          'taxonomy' => get_query_var('taxonomy'),
          'post_type' => get_query_var('post_type'),
          'category' => get_query_var('category'),
          'posts_per_page' => $this->args['query']['posts_per_page'],
          'template' => $this->template,
        ),
        'post' => $this->post,
      );


      if (isset($_GET['dump'])) {
        var_dump($context['archive']['posts'][0]->featuredImage);
      }

			$this->timber->addContext($context);

      return parent::get();
    }

    /**
     * get_the_archive_title
     *
     * @return void
     */
    public static function get_the_archive_title() {
      if ( \is_category() ) {
        /* translators: Category archive title. 1: Category name */
        $title = single_cat_title ('', false);
      } elseif ( \is_tag() ) {
        /* translators: Tag archive title. 1: Tag name */
        $title = single_tag_title ('', false);
      } elseif ( \is_author() ) {
        /* translators: Author archive title. 1: Author name */
        $title = get_the_author ();
      } elseif ( \is_year() ) {
        /* translators: Yearly archive title. 1: Year */
        $title = get_the_date (_x ('Y', 'yearly archives date format'));
      } elseif ( \is_month() ) {
        /* translators: Monthly archive title. 1: Month name and year */
        $title = get_the_date (_x ('F Y', 'monthly archives date format'));
      } elseif ( \is_day() ) {
        /* translators: Daily archive title. 1: Date */
        $title = get_the_date (_x ('F j, Y', 'daily archives date format'));
      } elseif ( \is_tax( 'post_format' ) ) {
        if ( \is_tax( 'post_format', 'post-format-aside' ) ) {
            $title = _x( 'Asides', 'post format archive title' );
        } elseif ( \is_tax( 'post_format', 'post-format-gallery' ) ) {
            $title = _x( 'Galleries', 'post format archive title' );
        } elseif ( \is_tax( 'post_format', 'post-format-image' ) ) {
            $title = _x( 'Images', 'post format archive title' );
        } elseif ( \is_tax( 'post_format', 'post-format-video' ) ) {
            $title = _x( 'Videos', 'post format archive title' );
        } elseif ( \is_tax( 'post_format', 'post-format-quote' ) ) {
            $title = _x( 'Quotes', 'post format archive title' );
        } elseif ( \is_tax( 'post_format', 'post-format-link' ) ) {
            $title = _x( 'Links', 'post format archive title' );
        } elseif ( \is_tax( 'post_format', 'post-format-status' ) ) {
            $title = _x( 'Statuses', 'post format archive title' );
        } elseif ( \is_tax( 'post_format', 'post-format-audio' ) ) {
            $title = _x( 'Audio', 'post format archive title' );
        } elseif ( \is_tax( 'post_format', 'post-format-chat' ) ) {
            $title = _x( 'Chats', 'post format archive title' );
        }
      } elseif ( \is_post_type_archive() ) {
        /* translators: Post type archive title. 1: Post type name */
        $title = post_type_archive_title ('', false);
      } elseif (\is_tax ()) {
        $tax = get_taxonomy (get_queried_object ()->taxonomy);
        /* translators: Taxonomy term archive title. 1: Taxonomy singular name, 2: Current taxonomy term */
        $title = sprintf( __( '%1$s: %2$s' ), $tax->labels->singular_name, single_term_title( '', false ) );
      } else {
        $title = __( 'Archives' );
      }
      return $title;
    }

    /**
     * addMorePostData
     *
     * @return void
     */
    public function addMorePostData() {
      global $wp_query;
      $found_posts = $wp_query->found_posts;
      $count = [];

      if ($found_posts !== null && $this->args['query']['posts_per_page'] !== null ) {
        if( 0 < ((int)$found_posts - (int)$this->args['query']['posts_per_page']) ) {
          $more = 1;
        } else {
          $more = 0;
        }
      }
      $this->archive_more = $more;
      $this->archive_found = $found_posts;;
    }
  }
