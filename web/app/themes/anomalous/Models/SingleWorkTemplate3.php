<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class SingleWorkTemplate3 extends SingleWork {

    /**
    *   @method __construct
    *   @return get
    **/
    public function __construct ($args) {
      parent::__construct($args);
    }

    /**
    *   @method get
    *   @return parent::get()
    *
    *
    **/
    public function get() {

      $context = array(
        'ContentImage' => $this->getContentImage(),
        'gallery' => $this->getGallery('gallery'),
        'TestionialParalax' => $this->getTestionialParalax(),
      );
      
      if (isset($_GET['dump'])) {
        die(var_dump($context));
      }

      $this->timber->addContext($context);

      return parent::get();
    }

    private function getTestionialParalax() {
      return Single::getProgressiveImageLoadingObj(
        get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialParalaxImage_id', true)
      );
      // get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialContent', true)
      // get_post_meta ($this->post->ID, CustomFields::$prefix . 'testimonialAuthor', true)
    }

    private function getContentImage() {
      return Single::getProgressiveImageLoadingObj(
        get_post_meta ($this->post->ID, CustomFields::$prefix . 'contentImage_id', true)
      );
    }
  }

  