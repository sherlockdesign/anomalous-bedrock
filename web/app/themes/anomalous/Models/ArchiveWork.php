<?php

  namespace Models;

  use ContentTypes\CustomFields as CustomFields;

  class ArchiveWork extends Archive {
    
    /**
     * __construct
     *
     * @param  mixed $args
     *
     * @return void
     */
    public function __construct ($args) {
      // $this->featuredImageSize = 'tiny';
      
      parent::__construct($args);
    }
    
    /**
     * get
     *
     * @return void
     */
    public function get() {
      $context = array(
        'workCategoriesMenu' => new \TimberMenu('work-categories'),
      );
  
      if (isset($_GET['dump'])) {
       var_dump($context);
      }
  
      $this->timber->addContext($context);

      return parent::get();
    } 
  }
