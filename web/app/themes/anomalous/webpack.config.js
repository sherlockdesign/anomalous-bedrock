const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const BrowserSyncPlugin = require('browser-sync-webpack-plugin')

module.exports = {
  entry: {
    main : "./src/index",
    home : "./src/components/templates/frontpage/index",
    services : "./src/components/templates/page-service/index",
    'archive-work' : "./src/components/templates/archive-work/index",
    'space-page' : "./src/components/templates/page-space/index",
    'style-guide' : "./src/components/templates/style-guide/index",
    'work-template-1': "./src/components/templates/single-work-1/index",
    'work-template-2': "./src/components/templates/single-work-2/index",
    'work-template-3': "./src/components/templates/single-work-3/index",
    'wp-admin': "./src/components/templates/wp-admin/index"
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.[name].js"
  },
  optimization: {
    splitChunks: {
      name: true,
    }
  },
  // devServer: {
  //   historyApiFallback: true,
  //   compress: true,
  //   port: 9000,
  //   https: 'https://develop.f3architects.com'.indexOf('https') > -1 ? true : false,
  //   publicPath: '/dist/',
  //   proxy: {
  //     '*': {
  //       'target': 'https://develop.f3architects.com',
  //       'secure': false,
  //       changeOrigin: true
  //     },
  //     '/': {
  //       target: 'https://develop.f3architects.com',
  //       secure: false,
  //       changeOrigin: true
  //     }
  //   },
  // },
  plugins: [
    new CopyWebpackPlugin([{
      from: 'src/img/png/',
      to: 'img/png'
    }]), 
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "css/[name].css",
      chunkFilename: "css/[contenthash].css",
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
    new BrowserSyncPlugin( {
        proxy: 'https://develop2.anomalous.com',
        files: [
          '**/*.php'
        ],
        reloadDelay: 0,
        open: false
      }
    )
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules)/,
        use: {
          loader: 'babel-loader',
          options: {
              presets: ['env']
          }
        }
      },
      {
        test: /\.scss$/,
        use: [
        MiniCssExtractPlugin.loader,
        {
          loader: "css-loader",
        },
        "resolve-url-loader",
        {
          loader: 'sass-loader',
          options: {
            sourceMap: true,
            sourceMapContents: true
          }
        }
      ]},
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: '../font/'
        }
      },
      { 
        test: /\.svg$/, 
        loader: 'svg-inline-loader' 
      },
      {
        test: /\.(png|jp(e*)g)$/,
        exclude: [
          path.resolve(__dirname, 'src/img/*'),
          path.resolve(__dirname, 'src/svg/*'),
        ],
        include: [
          path.resolve(__dirname, 'src/img/dataURL'),
        ],
        use: [{
          loader: 'url-loader',
          options: {
            // limit: 8000, // Convert images < 8kb to base64 strings
            name: 'img/[hash].[ext]'

          }
        }]
      },
      {
        test: /\.(png|jp(e*)g|)$/,
        exclude: [
          path.resolve(__dirname, 'src/img/dataURL'),
        ],
        use: [{
          loader: 'file-loader',
          options: {
            // limit: 8000, // Convert images < 8kb to base64 strings
            name: 'img/[name].[ext]'
          }
        }]
      }
    ]
  }
}

