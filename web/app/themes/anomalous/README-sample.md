# Sherlock — WordPress Starter Theme

> The `master` branch tracks starter development and it's not a stable code. If you want a stable version, use the [`production`](//github.com/ljsherlock/sherlock-wp-theme/tree/production) branch.

[![Build Status](https://travis-ci.org/tonik/theme.svg?branch=develop)](https://travis-ci.org/tonik/theme) [![Packagist](https://img.shields.io/packagist/dt/tonik/theme.svg)](https://github.com/tonik/theme) [![license](https://img.shields.io/github/license/tonik/theme.svg)](https://github.com/tonik/theme)

### Sherlock is a WordPress Starter Theme which makes WordPress development sane.

Here's what's included:

- Utilizes PHP [Namespaces](http://php.net/manual/pl/language.namespaces.php)
- Oriented for building in a Object Oriented MVC manner.
- [Twig Templating](https://twig.symfony.com/) using [Timber](https://github.com/timber/timber)
- [ES6](https://babeljs.io/learn-es2015/) for JavaScript
- [SASS](http://sass-lang.com/) preprocessor for CSS
- [Webpack](https://webpack.js.org/) for managing, compiling and optimizing theme's asset files

### Requirements

Sherlock Starter Theme follows [WordPress recommended requirements](https://wordpress.org/about/requirements/). Make sure you have all these dependences installed before moving on:

- WordPress >= 4.7
- PHP >= 7.0
- [Composer](https://getcomposer.org)
- [Node.js](https://nodejs.org)

## Theme installation

Install Sage using Composer from your WordPress themes directory (replace `your-theme-name` below with the name of your theme):

```shell
# @ app/themes/ or wp-content/themes/
$ git clone ....
```

## Theme structure

```shell
themes/your-theme-name/                 # → Root of theme
├── ContentTypes/                       # → Theme Content Types
│   ├── CustomFields/                   # → CMB2 custom fields 
│   │   ├── CustomPost.php                  # → CPT Abstract Class
│   │   ├── FrontPage.php               
│   │   ├── Staff.php                   
│   │   └── ThemeOptions.php            
│   ├── Widgets/                        # → Widgets
│   │   ├── Mailchimp_Subscribe.php     
│   │   ├── Recent_Posts_All.php        
│   │   └── Widget.php                  
│   ├── Shortcodes/                     # → Shortcoces 
│   │   ├── Shortcode.php               
│   │   └── Button.php                  
│   ├── CustomPostTypes/                # → CPTs
│   │   ├── CustomPost.php               
│   │   └── Staff.php                  
│   ├── Ajax.php                        # → Ajax Actions 
│   ├── Blocks.php                      # → Gutenburg Blocks
│   ├── CustomFields.php                # → Custom fields register
│   ├── Customizer.php                  # → Customizer Controls
│   ├── Images.php                      # → Images Config
│   ├── Map.php                         # → Maps
│   ├── Menus.php                       # → Menu config
│   ├── Scripts.php                     # → Script registration
│   ├── Shortcodes.php                  # → Shortcodes registration
│   ├── Sidebar.php                     # → Sidebar registration
│   └── Widgets.php                     # → Widget registration
├── Controllers/                        # → Theme Controllers
│   ├── Archive.php                     
│   ├── Base.php                        
│   ├── Carousel.php                    
│   ├── FrontPage.php                   
│   ├── Page.php                        
│   ├── Search.php                      
│   ├── Shortcode.php                   
│   ├── Single.php                      
│   ├── RecentPostsAll.php              
│   └── Widget.php                      
├── Includes/                           # → Utility functions that will be called ƒrom inside the Model Class 
│   ├── CMB2/                           # → CMB2 Custom Fields 
│   ├── CMB2.php                        # → CMB2 Custom FIelds Registry
│   ├── Utils.php                       # → Utility functions
├── Models/                             # → Models
│   ├── Archive.php                     
│   ├── Base.php                        
│   ├── Carousel.php                    
│   ├── FrontPage.php                   
│   ├── Page.php                        
│   ├── Search.php                      
│   ├── Shortcode.php                   
│   ├── Single.php                      
│   ├── RecentPostsAll.php              
│   └── Widget.php                      
├── composer.json                       # → Autoloading for `app/` files
├── composer.lock                       # → Composer lock file (never edit)
├── dist/                               # → Built theme assets (never edit)
├── node_modules/                       # → Node.js packages (never edit)
├── package.json                        # → Node.js dependencies and scripts
├── src/                                # → Weboack source
│   ├── components/                     # → Styled/Functional Components 
│   │   ├── parts/                      # → Component part
│   │   │   └── part/                   # → example part
│   │   │   │   ├── part.twig           # → part javascript
│   │   │   │   ├── part.js             # → part javascript
│   │   │   │   └── part.scss           # → part scss
│   │   ├── templates/                  # → Component template
│   │   │   └── template/               # → example template
│   │   │   │   ├── template.twig       # → template javascript
│   │   │   │   ├── template.js         # → template javascript
│   │   │   │   └── template.scss       # → template scss
│   │   └── macros.twig/                  # → Component template
│   ├── css/                            # → fonts
│   ├── font/                           # → fonts
│   ├── img/                            # → imgs
│   ├── js/                             # → javascript
│   ├── sass/                           # → SCSS
│   ├── svg/                            # → SVG vectors
│   └── index.js                        # → main javascript file
├── style-guide/                        # → Style guide
│   ├── templates/                      # → Never manually edit
│   ├── config.rb                       # → Never manually edit
│   ├── page-style-guide-source.twig    # → Never manually edit
│   └── page-style-guide.twig           # → Never manually edit
├── index.php                           # → Never manually edit
├── screenshot.png                      # → Theme screenshot for WP admin
├── vendor/                             # → Composer packages (never edit)
├── Workers/                            # → Workers Directory
│   └── Timber.twig                     # → Timber Class
├── frontpage.php                       # → Frontpage template
├── home.php                            # → Default posts page (if not set to home)
├── index.php                           # → empty index.php (silence is golden)
├── archive.php                         # → Archive template
├── search.php                          # → Search template
└── style.css                           # → Set theme name etc (no css)
```

## Theme setup

 - Name theme in style.css
 - Replace screenshot.png with your own
 - All other edits

## Theme development

* Run `yarn` from the theme directory to install dependencies

### Build commands

* `yarn start` — Start webpack watch

## Contributing

Great that you are considering supporting the project. You have a lot of ways to help us grow. We appreciate all contributions, even the smallest.

- Report an issue
- Propose a feature
- Send a pull request
- Star project on the [GitHub](https://github.com/tonik/tonik)
- Tell about project around your community

## License

The Sherlock Starter Theme is licensed under the [MIT license](http://opensource.org/licenses/MIT).


# Rough Hands

## Description

This is a the build of a the frontpage as part of a custom theme for the Rough Hands barbershop

For a demo please go to [codeable.ljsherlock.com](codeable.ljsherlock.com)

Note: since this is a custom theme it takes more 'custom' data and settings to
setup, so I recommend going to the above link, as opposed to downloading and
installing this first.

## Installation

1. Clone this repository to your 'themes' folder
2. Run `npm install` to install javascript modules
3. In your admin panel, go to Appearance -> Themes
4. Click 'Activate' to use your new theme right away.
5. Navigate to Appearance > Customize in your admin panel and customize to taste.
