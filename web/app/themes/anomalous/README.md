# Anomalous WordPress theme

## Theme installation

Clone the repository into the themes folder of you WordPress install.

```shell
# @ app/themes/ or wp-content/themes/
$ git clone ....
```

## Theme structure

```shell
themes/your-theme-name/                 # → Root of theme
├── ContentTypes/                       # → Theme Content Types
│   ├── CustomFields/                   # → CMB2 custom fields 
│   │   ├── CustomPost.php                  # → CPT Abstract Class
│   │   ├── FrontPage.php               
│   │   ├── Staff.php                   
│   │   └── ThemeOptions.php            
│   ├── Widgets/                        # → Widgets
│   │   ├── Mailchimp_Subscribe.php     
│   │   ├── Recent_Posts_All.php        
│   │   └── Widget.php                  
│   ├── Shortcodes/                     # → Shortcoces 
│   │   ├── Shortcode.php               
│   │   └── Button.php                  
│   ├── CustomPostTypes/                # → CPTs
│   │   ├── CustomPost.php               
│   │   └── Staff.php                  
│   ├── Ajax.php                        # → Ajax Actions 
│   ├── Blocks.php                      # → Gutenburg Blocks
│   ├── CustomFields.php                # → Custom fields register
│   ├── Customizer.php                  # → Customizer Controls
│   ├── Images.php                      # → Images Config
│   ├── Map.php                         # → Maps
│   ├── Menus.php                       # → Menu config
│   ├── Scripts.php                     # → Script registration
│   ├── Shortcodes.php                  # → Shortcodes registration
│   ├── Sidebar.php                     # → Sidebar registration
│   └── Widgets.php                     # → Widget registration
├── Controllers/                        # → Theme Controllers
│   ├── Archive.php                     
│   ├── Base.php                        
│   ├── Carousel.php                    
│   ├── FrontPage.php                   
│   ├── Page.php                        
│   ├── Search.php                      
│   ├── Shortcode.php                   
│   ├── Single.php                      
│   ├── RecentPostsAll.php              
│   └── Widget.php                      
├── Includes/                           # → Utility functions that will be called ƒrom inside the Model Class 
│   ├── CMB2/                           # → CMB2 Custom Fields 
│   ├── CMB2.php                        # → CMB2 Custom FIelds Registry
│   ├── Utils.php                       # → Utility functions
├── Models/                             # → Models
│   ├── Archive.php                     
│   ├── Base.php                        
│   ├── Carousel.php                    
│   ├── FrontPage.php                   
│   ├── Page.php                        
│   ├── Search.php                      
│   ├── Shortcode.php                   
│   ├── Single.php                      
│   ├── RecentPostsAll.php              
│   └── Widget.php                      
├── composer.json                       # → Autoloading for `app/` files
├── composer.lock                       # → Composer lock file (never edit)
├── dist/                               # → Built theme assets (never edit)
├── node_modules/                       # → Node.js packages (never edit)
├── package.json                        # → Node.js dependencies and scripts
├── src/                                # → Weboack source
│   ├── components/                     # → Styled/Functional Components 
│   │   ├── parts/                      # → Component part
│   │   │   └── part/                   # → example part
│   │   │   │   ├── part.twig           # → part javascript
│   │   │   │   ├── part.js             # → part javascript
│   │   │   │   └── part.scss           # → part scss
│   │   ├── templates/                  # → Component template
│   │   │   └── template/               # → example template
│   │   │   │   ├── template.twig       # → template javascript
│   │   │   │   ├── template.js         # → template javascript
│   │   │   │   └── template.scss       # → template scss
│   │   └── macros.twig/                  # → Component template
│   ├── css/                            # → fonts
│   ├── font/                           # → fonts
│   ├── img/                            # → imgs
│   ├── js/                             # → javascript
│   ├── sass/                           # → SCSS
│   ├── svg/                            # → SVG vectors
│   └── index.js                        # → main javascript file
├── style-guide/                        # → Style guide
│   ├── templates/                      # → Never manually edit
│   ├── config.rb                       # → Never manually edit
│   ├── page-style-guide-source.twig    # → Never manually edit
│   └── page-style-guide.twig           # → Never manually edit
├── index.php                           # → Never manually edit
├── screenshot.png                      # → Theme screenshot for WP admin
├── vendor/                             # → Composer packages (never edit)
├── Workers/                            # → Workers Directory
│   └── Timber.twig                     # → Timber Class
├── frontpage.php                       # → Frontpage template
├── home.php                            # → Default posts page (if not set to home)
├── index.php                           # → empty index.php (silence is golden)
├── archive.php                         # → Archive template
├── search.php                          # → Search template
└── style.css                           # → Set theme name etc (no css)
```

## Theme setup

 - Name theme in style.css
 - Replace screenshot.png with your own
 - All other edits

## Theme development

* Run `yarn` from the theme directory to install dependencies

### Build commands

* `yarn start` — Start webpack watch

## Contributing

Great that you are considering supporting the project. You have a lot of ways to help us grow. We appreciate all contributions, even the smallest.

- Report an issue
- Propose a feature
- Send a pull request
- Star project on the [GitHub](https://github.com/tonik/tonik)
- Tell about project around your community

## License

The Sherlock Starter Theme is licensed under the [MIT license](http://opensource.org/licenses/MIT).