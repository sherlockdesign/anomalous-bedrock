<?php
/**
* Template Name: Video Gallery Template
* Template Post Type: work
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

$SingleWorkVideoGallery = new Controllers\SingleWorkVideoGallery();
$SingleWorkVideoGallery->show();
