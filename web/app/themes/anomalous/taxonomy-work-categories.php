<?php

$args = array(
  'query' => array(
    'post_type' => 'work',
    'posts_per_page' => 15,
    'tax_query' => array(
      'relation' => 'AND',
      array(
        'taxonomy' => 'work-categories',
        'field' => 'slug',
        'terms' => get_query_var('term'),
      ),
      array(
        'taxonomy' => 'portfolio',
        'terms' => array(68),
        'operator' => 'NOT IN'
      )
    )
  )
);

$workArchive = new Controllers\ArchiveWork($args);
$workArchive->template = 'templates/tax-work-categories/tax-work-categories';
$workArchive->show();