<?php

$loader = require __DIR__ . '/vendor/autoload.php';

/**
 * Additional features to allow styling of the templates.
 */

 /**
  * Count our number of active panels.
  *
  * Primarily used to see if we have any panels active, duh.
  */
 function roughhands_panel_count() {
 	$panels = array( '1', '2', '3', '4' );
 	$panel_count = 0;
 	foreach ( $panels as $panel ) {
 		if ( get_theme_mod( 'panel_' . $panel ) ) {
 			$panel_count++;
 		}
 	}
 	return $panel_count;
 }

 /**
  * Checks to see if we're on the homepage or not.
  */
 function roughhands_is_frontpage() {
 	return ( is_front_page() && ! is_home() );
 }

 /**
  * Custom Active Callback to check for page.
  */
 function roughhands_is_page() {
 	return (is_page());
 }
 
require_once 'Includes/constants.php';

// add_action('init', array( '\ContentTypes\Shortcodes', 'setup' ));

// REMOVE WP EMOJI
/*
* note sure whether to keep this, or not.
*/
remove_action ('wp_head', 'print_emoji_detection_script', 7);
remove_action ('wp_print_styles', 'print_emoji_styles');

remove_action ('admin_print_scripts', 'print_emoji_detection_script');
remove_action ('admin_print_styles', 'print_emoji_styles');

/*
	* Let WordPress manage the document title.
	* By adding theme support, we declare that this theme does not use a
	* hard-coded <title> tag in the document head, and expect WordPress to
	* provide it for us.
	*/
add_theme_support ('title-tag');

// Add theme support for Custom Logo.
add_theme_support ('custom-logo', array(
	'width'       => 250,
	'height'      => 250,
	'flex-width'  => true,
));

ContentTypes\Sidebar::register();
ContentTypes\Widgets::register();

Includes\CMB2::init();
ContentTypes\CustomFields::register();

ContentTypes\Scripts::register();
ContentTypes\Menus::register();
ContentTypes\CustomPostTypes\Space::registerPostType();

add_action( 'init', array('ContentTypes\CustomPostTypes\TeamMembers', 'registerPostType'));
add_action( 'init', array('ContentTypes\CustomPostTypes\Work', 'registerPostType'));

ContentTypes\Customizer::setup();
ContentTypes\Images::setup();
ContentTypes\Ajax::setup();

function mytheme_tinymce_settings( $settings ) {
	//First, we define the styles we want to add in format 'Style Name' => 'css classes'
	$classes = array(
		__('Test style 1', 'mytheme') => '.teststyle1',
		__('Test style 2', 'mytheme') => '.teststyle2',
		__('Test style 3', 'mytheme') => '.teststyle3',
	);

	//Delimit styles by semicolon in format 'Title=classes;' so TinyMCE can use it
	if ( ! empty( $settings['theme_advanced_styles'] ) ) {
		$settings['theme_advanced_styles'] .= ';';
	} else {
		//If there's nothing defined yet, define it
		$settings['theme_advanced_styles'] = '';
	}

	//Loop through our newly defined classes and add them to TinyMCE
	$class_settings = '';
	foreach ( $classes as $name => $value ) {
		$class_settings .= "{$value}={$name};";
	}

	//Add our new class settings to the TinyMCE $settings array
	$settings['theme_advanced_styles'] .= trim( $class_settings, '; ' );
	$settings['style_formats'] = json_encode(array(
		array(
			'title' => 'Block Heading',
			'block' => 'h1',
			'classes' => 'db f1-xl f2-m f2-l f3 lh-solid din fw7 ttu tracked measure-wide mb2',
		),
		array(
			'title' => 'Block Heading, Right aligned',
			'block' => 'h1',
			'classes' => 'db f1-xl f2-m f2-l f3 lh-solid din fw7 ttu tracked measure-wide mb2 ml-auto-ns mr-0-ns tr-ns',
		),
		array(
			'title' => 'Paragraph',
			'block' => 'span',
			'classes' => 'f4 f3-xl montserrat fw3 lh-copy measure-wide db',
		),
		array(
			'title' => 'Paragraph, Right aligned',
			'block' => 'span',
			'classes' => 'f4 f3-xl montserrat fw3 lh-copy measure-wide db ml-auto-ns mr-0-ns tr-ns',
		),
		array(
			'title' => 'Paragraph (centered)',
			'block' => 'span',
			'classes' => 'f4 f3-xl montserrat fw3 lh-copy measure-wide db center',
		),
		array(
			'title' => 'Large Paragraph',
			'block' => 'span',
			'classes' => 'f3-xl f3-l f4-m f5 montserrat fw3 lh-copy measure-wide db',
		),
		array(
			'title' => 'Large Paragraph, Right Aligned',
			'block' => 'span',
			'classes' => 'f3-xl f3-l f4-m f5 montserrat fw3 lh-copy measure-wide db ml-auto-ns mr-0-ns tr-ns',
		),
		array(
			'title' => 'Large Paragraph (centered)',
			'block' => 'span',
			'classes' => 'f3-xl f3-l f4-m f5 montserrat fw3 lh-copy measure-wide db center',
		),
		array(
			'title' => 'Quote',
			'block' => 'blockquote',
			'classes' => 'f3-xl f3-l f4-m f5 montserrat fw3 lh-copy measure-wide db persian-green mv4 pl4 mh0 bl bw3 b--persian-green',
		), 
		array(
			'title' => 'Small Text',
			'block' => 'span',
			'classes' => 'f6 montserrat fw3 db mb1',
		), 

		// array(
		// 	'title' => 'Right aligned block',
		// 	'block' => 'span',
		// 	'classes' => 'ml-auto-ns mr-0-ns tr-ns',
		// ), 
		// array(
		// 	'title' => 'Center aligned block',
		// 	'block' => 'span',
		// 	'classes' => 'tc center',
		// ),
		// array(
		// 	'title' => 'Measure',
		// 	'block' => 'span',
		// 	'classes' => 'measure',
		// ), 
		// array(
		// 	'title' => 'Measure Wide',
		// 	'block' => 'span',
		// 	'classes' => 'measure-wide',
		// ), 
		array(
			'title' => 'Image',
			'block' => 'span',
			'classes' => 'f4 f3-xl measure-wide db',
		), 
		array(
			'title' => 'Image Columns',
			'block' => 'span',
			'classes' => 'f4 f3-xl measure-wide db image-columns',
		), 
		array(
			'title' => 'Green',
			'block' => 'span',
			'classes' => 'persian-green',
			'styles' => array(
				'color' => '#00ABAA'
			),
		), 
		array(
			'title' => 'White',
			'block' => 'span',
			'classes' => 'off-white',
			'styles' => array(
				'color' => '#FEFEFE'
			),
		), 
	));
	return $settings;
}
add_filter( 'tiny_mce_before_init', 'mytheme_tinymce_settings' );

function plugin_mce_css( $mce_css ) {
	if ( ! empty( $mce_css ) )
		$mce_css .= ',';

	$mce_css .= plugins_url( 'editor.css', __FILE__ );

	return $mce_css;
}
add_filter( 'mce_css', 'plugin_mce_css' );

function myplugin_tinymce_buttons( $buttons ) {
	//Add style selector to the beginning of the toolbar
	array_unshift( $buttons, 'styleselect' );

	return $buttons;
}
add_filter( 'mce_buttons_2', 'myplugin_tinymce_buttons' );

add_filter('show_admin_bar', '__return_false');