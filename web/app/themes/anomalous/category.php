<?php

$args = array(
  'query' => array(
    'post_type' => 'post',
    'posts_per_page' => 15,
    'tax_query' => array(
      array(
        'taxonomy' => 'category',
        'terms' => get_query_var('cat'),
      ),
    ),
   )
);

$archive = new Controllers\Archive($args);
$archive->show();

