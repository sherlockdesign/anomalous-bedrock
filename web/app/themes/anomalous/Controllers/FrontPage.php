<?php

namespace Controllers;

class FrontPage extends Base {
  public $modelName = "FrontPage";
  public $template = "templates/frontpage/frontpage";
}
