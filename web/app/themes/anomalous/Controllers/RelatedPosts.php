<?php

namespace Controllers;

class RelatedPosts extends Base {
  public $modelName = "RelatedPosts";
  public $template = 'templates/related-posts/related-posts';
}
