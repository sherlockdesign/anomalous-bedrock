<?php

namespace Controllers;

class Carousel extends Page {
  public $modelName = "Carousel";
  public $template = "parts/carousel/carousel";
}
