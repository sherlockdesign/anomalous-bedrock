<?php

namespace Controllers;

class Page extends Base {
  public $modelName = "Page";
  public $template = 'templates/page/page';
}
