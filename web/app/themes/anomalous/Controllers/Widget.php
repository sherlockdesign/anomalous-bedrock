<?php

namespace Controllers;

class Widget extends Base {
  public $modelName = "Widget";
  public $template = "parts/widget/widget";
}
