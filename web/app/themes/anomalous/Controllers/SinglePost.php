<?php

namespace Controllers;

class SinglePost extends Base {
  public $modelName = "SinglePost";
  public $template = 'templates/single-post/single-post';
}
