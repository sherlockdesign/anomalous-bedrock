<?php

namespace Controllers;

class StyleGuide extends Page {
  public $modelName = "StyleGuide";
  public $template = "templates/style-guide/style-guide";
}
