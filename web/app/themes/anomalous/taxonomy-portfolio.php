<?php

$args = array(
  'query' => array(
    'post_type' => 'work',
    'posts_per_page' => 15,
    'tax_query' => array(
      array(
        'taxonomy' => 'portfolio',
        'field' => 'slug',
        'terms' => get_query_var('term'),
      )
    )
   )
);

$workArchive = new Controllers\ArchiveWork($args);
$workArchive->template = 'templates/tax-work-categories/tax-work-categories';
$workArchive->show();