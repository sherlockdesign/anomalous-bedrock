import mapStyles from './map-styles.json'

class Map {
  constructor () {
    this.mapElement = document.getElementById( "map" )
    this.mapElementProperties = {
      center: { lat: 51.532163, lng: -0.108151 },
      zoom: 16,
      //  mapTypeId: window.google.maps.MapTypeId.ROADMAP,
      disableDefaultUI: true,
      zoomControl: true,
      scrollwheel: false,
      styles: mapStyles,
      disableDoubleClickZoom: true,
      // backgroundColor: "#F7F2F9",
    }
  }

  init () {
    if(this.mapElement !== null) {

      this.mapElement.style.height = "100%"
      var map = new window.google.maps.Map(this.mapElement, this.mapElementProperties ),
      marker = new window.google.maps.Marker({
        position: { lat: 51.532163, lng: -0.108151 },
        map: map,
        title: 'F3 Architects',
        // icon: '', #link
      }),
      center  = map.getCenter()
  
      window.google.maps.event.addDomListener(window, 'resize', function () {
          map.setCenter(center)
      })
    }
  }
}

export default Map