
class Squares {

  /**
   * Load the GIFs from the data-gif attribute
   */
  static loadGifs () {
    let patternsNodelist = document.querySelectorAll('.square svg pattern')
    // loop through
    var i = 1;
    for (const pattern of patternsNodelist) {
      let state = pattern.getAttribute('state')
      if(state !== 'loaded') {
        let dataGIF = pattern.getAttribute('data-gif')
        var img = new Image();
        img.src = dataGIF     
        img.onload = function () {
          pattern.innerHTML = `<image xlink:href="${dataGIF}" width="300" height="200"></image>`
          pattern.setAttribute('state', 'loaded')
        }
      }
      i++
    }
  }

}

export default Squares