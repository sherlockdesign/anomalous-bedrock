
import State from '../../../js/state'
import observeMutations from '../../../js/observeMutations'
import DOMUtility from '../../../js/dom'

class Video {
  constructor () {
    this.videoEls = document.querySelectorAll('.work-video')

    this.initiateVideos()
    this.observeDOM()
  }

  playPauseVideo (videoBlock) {
    if (videoBlock !== null) {

      var videoElement = videoBlock.querySelector("video"),
      videoButton = videoBlock.querySelector(".video__playpause")
  
      videoElement && videoButton && (videoElement.removeAttribute("controls"), 
      videoButton.addEventListener("click", (e) => {
        State.toggleState(videoBlock, 'playing', 'paused')
        if (videoElement.paused) {
          videoElement.play()
        } else {
          videoElement.pause()
        }
      }, !1))
    }
  }

  initiateVideos () {
    if (this.videoEls.length > 0) {
      
      for (const key in this.videoEls) {
        if (this.videoEls.hasOwnProperty(key)) {
          this.playPauseVideo(this.videoEls[key])
        }
      }
      console.log(`ANOMALOUS: added video event listeners`);
    }
    DOMUtility.setIframeVideoAspectRatioPad()
  }

  observeDOM () {
    this.observeDOM = new observeMutations()
    this.observeDOM.config.subtree = false
    this.observeDOM.callback = (mutationsList, observer) => { 
      for(var mutation of mutationsList) {
    
        let addedNodes = mutation.addedNodes
    
        if(addedNodes.length > 0) {
      
          for (var key in addedNodes ) {
    
            if (addedNodes[key].id == null) {          
            } else {
              if (addedNodes[key].id === 'workPost') {
                this.initiateVideos()
              }
            }
          }
        } 
      }
    }
    this.observeDOM.observe()
  }
}

export default Video