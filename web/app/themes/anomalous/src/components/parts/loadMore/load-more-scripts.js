
import Ajax from '../../../js/Ajax'
import State from '../../../js/state'

import progressiveImages from '../../../js/progressiveImages'

export class loadMore {
  static init() {
    let loadMore = document.querySelector('#loadMore')

    if (loadMore !== null) {
      var offset = loadMore.getAttribute('data-offset'),
      total = loadMore.getAttribute('data-total')
    
      if (parseInt(total) < parseInt(offset)) {
        loadMore.setAttribute('data-more', 0)
        loadMore.classList.add('state')
        loadMore.classList.add('state--hide')
      }
    
      loadMore.addEventListener('click', function (event) {

        event.preventDefault()
        // get all the attrs
        var target = event.currentTarget,
        more = target.getAttribute('data-more'),
        post_type = target.getAttribute('data-post-type'),
        posts_per_page = target.getAttribute('data-posts-per-page'),
        taxonomy = target.getAttribute('data-taxonomy'),
        category = target.getAttribute('data-category'),
        tag = target.getAttribute('data-tag'),
        term = target.getAttribute('data-term'),
        template = target.getAttribute('data-template')
    
        target.classList.add('state--hide')
    
        // creat the JSON obj
        var json_string = {
          action: 'load_more',
          offset: offset,
          total: total,
          post_type: post_type,
          posts_per_page: posts_per_page,
          taxonomy: taxonomy,
          category: category,
          tag: tag,
          term: term,
          template: template
        }
    
        if (more == 1) {

          Ajax.put(window.ajax_url, json_string, function (response) {

            let archiveMain = document.getElementById('archive')

            for (const post of JSON.parse(response)) {
              let postElement = document.createElement('div')
              postElement.setAttribute('class', 'col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 post pa2 scale-grow block-reveal')
              postElement.innerHTML = post
              archiveMain.appendChild(postElement)
              // break
            }
    
            // update the button data
            offset = parseInt(offset) + parseInt(posts_per_page)
            
            if(offset == total) {
              State.setState(document.getElementById('loadMore'), 'hide')
            }

            target.setAttribute('data-offset', offset)
            target.classList.remove('dn')

            if (total < offset) {
              target.setAttribute('data-more', 0)
              target.classList.remove('flex')
              // State.setState(target, '')
              target.setAttribute('disabled', 'disabled')
            }
          })
        }
      })
    }
  }
}

export class ObserveNewPosts {
  constructor () {///
    // Select the node that will be observed for mutations
    this.targetNode = document.getElementById('archive')

    // Options for the observer (which mutations to observe)
    this.config = { attributes: true, childList: true, subtree: true }
  }

  // Callback function to execute when mutations are observed
  callback (mutationsList, observer) {
    for(var mutation of mutationsList) {
      
      if(mutation.addedNodes.length > 0) {
        if (mutation.addedNodes) {
          // console.log(`(mutation.addedNodes[1]) ${mutation.addedNodes[1]}`)
          // console.log(mutation.addedNodes)
          // console.log(mutation)

          for (const node of mutation.addedNodes) {
            if (node.classList.contains('post')) { 
              let image = node.querySelector('.progressive-image-placeholder')
              progressiveImages.progressivelyLoadImage(image)
              
              State.setState(node, 'show')
            }
          }
        }
      }
    }
  }

  observe () {

    if (this.targetNode !== null) {

      // Create an observer instance linked to the callback function
      var observer = new MutationObserver(this.callback)

      // Start observing the target node for configured mutations
      observer.observe(this.targetNode, this.config)
    }
  }
  stopObserve () {
    observer.disconnect()
  }
}