
import 'owl.carousel'
import observeMutations from './../../../js/observeMutations'

class ClientsCarousel {
  constructor () {
    this.Owlconfig = {
      nav: false,
      dots: true,
      dotsEach: true,
      loop: true,
      // navClass: ['owl-prev ml4 f3 outline-0 grow', 'owl-next mr4 f3 outline-0 grow'],
      dotsClass: 'owl-dots mw8 center bg-white-40 mt3 flex w-75',
      responsive: {
        0: {
           items:1
        },
        768: {
           items:3
        },
        1024: {
           items:6
        }
      },
      items: 6,
      pullDrag: false,
      responsiveRefreshRate: 0,
      autoplay: true, 
      slideTransition: 'linear',
      autoplayTimeout: 1500,
      smartSpeed: 1500,
      autoplayHoverPause: true,
    }

    this.carouselElement = $('.clients-carousel')
    
    if(this.carouselElement.length !== 0) {
      this.carousel = this.carouselElement.owlCarousel(this.Owlconfig)
      console.debug('Clients Carousel: initiated')
    }

    this.mutations()
  }
  
  destroy () {
    if (typeof this.carousel !== 'undefined') {
      console.debug(this.carousel)
      this.carousel.trigger('destroy.owl.carousel')
      console.debug(this.carousel)
      console.debug('Clients Carousel: destroyed')
    } else {
      return false
    }
  }

  mutations () {
    const observeDOM = new observeMutations()
    observeDOM.config.subtree = false
    observeDOM.callback = function(mutationsList, observer) { 
      
      for(var mutation of mutationsList) {
    
        let addedNodes = mutation.addedNodes
    
        if(addedNodes.length > 0) {
      
          for (var key in addedNodes ) {
    
            if (addedNodes[key].id == null) {          
            } else {
              if (addedNodes[key].id === 'servicePage' || addedNodes[key].id === 'spacePage') {
                $('.clients-carousel').owlCarousel(this.Owlconfig)
                console.warn('Owl Carousel Initiated.')
              }
            }
          }
        } 
      }
    }
    observeDOM.observe()
  }
}

export default ClientsCarousel