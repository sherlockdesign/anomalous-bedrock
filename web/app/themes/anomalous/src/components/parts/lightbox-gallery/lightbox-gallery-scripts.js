import magnificPopup from 'magnific-popup'
import 'magnific-popup/src/css/main.scss'

import fullscreenIcon from '../../../svg/fullscreen.svg'
import videoIcon from '../../../svg/play-icon-white.svg'

class LightboxGallery {

  static init () {
    const gallery = document.getElementById('gallery');

    /**
     * There should always be a check to see if the element exist before
     * doing anything! You CANNOT asssume that the element will always be
     * there.
     */

    if(gallery !== null) {
      console.debug('ANOMALOUS: Initiate the gallery.');
      
      [].forEach.call(document.querySelectorAll('.lightbox-gallery .lightbox-link'), function(el) {
        el.innerHTML = fullscreenIcon
      });

      [].forEach.call(document.querySelectorAll('.lightbox-video-gallery .lightbox-video-link'), function(el) {
        el.innerHTML = videoIcon
      });
      
      [].forEach.call(document.querySelectorAll('.lightbox-gallery .lightbox-link svg'), function(el) {
        el.style.display = 'block';
        el.style.height = '100%';
      });
      
      $('.lightbox-gallery a').magnificPopup({
        // removalDelay: 300,
        // mainClass: 'animated zoomIn',
        type: 'image',
        closeBtnInside: false,
        closeOnContentClick: true,
        image: {
          verticalFit: true,
          titleSrc: ''
        },
        gallery: {
          enabled: true, // set to true to enable gallery
          preload: [0,2], // read about this option in next Lazy-loading section
          navigateByImgClick: true,
          arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow white icon-arrow icon-arrow-%dir%"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129" fill="currentColor" class="mfp-prevent-close grow"><g><path d="m40.4,121.3c-0.8,0.8-1.8,1.2-2.9,1.2s-2.1-0.4-2.9-1.2c-1.6-1.6-1.6-4.2 0-5.8l51-51-51-51c-1.6-1.6-1.6-4.2 0-5.8 1.6-1.6 4.2-1.6 5.8,0l53.9,53.9c1.6,1.6 1.6,4.2 0,5.8l-53.9,53.9z"/></g></svg></button>', // markup of an arrow button
          tPrev: 'Previous (Left arrow key)', // title for left button
          tNext: 'Next (Right arrow key)', // title for right button
          tCounter: '<span class="mfp-counter tc">%curr% / %total%</span>' // markup of counter
        }
      });
    } 

    $('.showreel-lightbox, .lightbox-video-gallery a').magnificPopup({
      type: 'iframe',
      closeBtnInside: false,
      gallery: {
        enabled: false, // set to true to enable gallery
      }
    });
    
  }
}

export default LightboxGallery