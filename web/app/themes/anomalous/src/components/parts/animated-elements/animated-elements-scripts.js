import Scroll from './../../../js/scroll'
import State from './../../../js/state'

import elements from './animated-elements';

let _elements = elements
let _elementsInPage = []

export default class AnimatedElements {

  constructor(develop) {
    _elementsInPage = this.findElementsInPage()
    if (develop === true) {
      console.groupCollapsed('Animatied Elements')
      console.table(this.elementsTable)
      console.groupEnd(); 
    }
  }

  get elements () {
    return _elementsInPage
  }

  // set: function(value) {
  //   temperature = value;
  //   archive.push({ val: temperature });
  // }

  set elements (value) {
    _elementsInPage = value
  }

  /**
   *  The first thing that we need to do is identify which elements are in this page.
   *  Loop through the list of IDs and create a new Array containing the elements
   *  that exist in the page.
   */
  findElementsInPage () {
    this.foundElementsInPage = []
    // this.elementsTable = []

    for (let element of _elements) {

      let elementNode = document.querySelector(element)

      if (elementNode !== null) {

        // this.elementsTable.push({
        //   id: element,
        //   inDocument: true,
        //   inViewport: (Scroll.isElementInViewPort(elementNode, true)) ? true : false,
        //   element: elementNode,
        // })

        this.foundElementsInPage.push(elementNode)

      } 
      // else {
        
        // this.elementsTable.push ({
        //   id: element,
        //   inDocument: false,
        // })

      // }

    }
    return this.foundElementsInPage
  }

  /**
   * @description Elements to be animatied on page-load
   */
  onLoadAnimations () {
    this.hiddenContentIds(
      this.elements
    )
  }

  onScrollAnimations () {
    this.scroll = new Scroll()

    this.scroll.AnimatedElements = new AnimatedElements()

    this.scroll.ScrollEventsCallback = function() {

      // this.AnimatedElements.hiddenContentIds(this.AnimatedElements.elements)

      // is the element in the viewport?
      // is the element already shown?
      // show element and then remove from the array.
      // if the array becomes empty, stop the function.
      // some more comments

      let elements = this.AnimatedElements.elements

      for (let key in elements) {

        let setState = State.setState,
        elementNode = elements[key],
        isElementInViewPort = Scroll.isElementInViewPort

        // Does the element exist?
        if (elementNode !== null) {

          // is the element in the viewport?
          if( isElementInViewPort(elementNode, true) ) {

            // Is the state not 'show'?
            if (elementNode.getAttribute('data-state') !== 'show') {
              
              // show element
              setState(elementNode, 'show')
              // Remove from array.
              elements.splice(key, 1)
            }
          }
        
        // Does not exist  
        } else {
          elements.splice(key, 1)
        }
      }
    }

    this.scroll.scrollEvents()
  }

  /**
   * 
   * @param {*} ids 
   * @description elements to be shown on scroll and in-viewport
   */
  hiddenContentIds (ids) {
    for (let key in ids) {
      let elementNode = ids[key]
      this.showElement(elementNode)
    }
  }

  showElement (elementNode) {
    if (elementNode !== null) {
      if( Scroll.isElementInViewPort(elementNode, true) ) {
        if (elementNode.getAttribute('data-state') !== 'show') {
          State.setState(elementNode, 'show')
          return true
          // console.log(`Animation: Start animating ${elementNode.id}`)
        }
      }
      return false
    }
    return false
  }
}

