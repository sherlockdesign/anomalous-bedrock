
import State from '../../../js/state.js'
import Ajax from '../../../js/Ajax'

class creditsModal {
  static init() {
    let creditsModalOpen = document.querySelector("#projectCredits")
    let creditsModalClose = document.getElementById('creditsModalClose')

    if (creditsModalOpen !== null ) {
      creditsModalOpen.addEventListener('click', function(e) {
        e.preventDefault();

        let creditsModal = document.getElementById('creditsModal')

        // Modal not already in the DOM
        if (creditsModal === null) { 

          // Get the modal with ajax
          let projectID = e.currentTarget.getAttribute('data-post-id')
          
          // creat the JSON obj
          var jsonString = {
            action: 'get_component',
            id: projectID,
            template: 'parts/credits-modal/credits-modal',
            state: 'show',
            modelName: 'SingleWork',
          }

          Ajax.put(window.ajax_url, jsonString, function (response) {
            document.querySelector('header').insertAdjacentHTML('afterend', response);
          })

        // is already in the DON
        } else {
          State.setState(creditsModal, 'show')
        }
    

      })
    }

    document.querySelector('body').addEventListener('click', function(event) {
      
      // This is not the ideal solution, but here it works because the target 
      // doesn't have many children.
      if (event.target.id === 'creditsModalClose' 
        || event.target.parentNode.id === 'creditsModalClose' ) {
        let creditsModel = document.getElementById('creditsModal')
      
        State.setState(creditsModel, 'hide')
        creditsModel.parentNode.removeChild(creditsModel)
      }
    });

  }
}

export default creditsModal