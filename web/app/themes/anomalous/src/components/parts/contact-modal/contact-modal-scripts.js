
import State from '../../../js/state.js'
import Map from './../map/map-scripts'

class ContactModal {

  constructor () {
    this.modal = document.getElementById('contactModal')
    this.contactModalOpen = document.getElementById("contactModalOpen")
    this.contactModalClose = document.getElementById('contactModalClose')
    
    if (this.contactModalOpen !== null && this.contactModalClose !== null) {
      this.contactModalOpen.addEventListener('click', (e) => this.open(e) )
      this.contactModalClose.addEventListener('click', (e) => this.close(e) )
    }
  }

  open (e) {
    State.setState(this.modal, 'show')
    if (this.map !== null) {
      this.map = new Map()
      this.map.init()
    }
  }

  close (e) {
    State.setState(this.modal, 'hide')
  }
}

export default ContactModal