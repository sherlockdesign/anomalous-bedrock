
import Scroll from '../../../js/scroll.js'
import State from '../../../js/state.js'

export default class Header {
  static scroll () {
    Scroll.scrollElementToggle(  
      document.getElementById('deskopHeader'),
      null,
      (elementToggle) => { State.setState(elementToggle, '') }, // scrolledToTopCallback
      null, // scrolledDownCallback
      (elementToggle) => { State.setState(elementToggle, 'hide') }, // scrolledPastElemCallback
      (elementToggle) => { State.setState(elementToggle, '') }, // scrolledUpAboveElemCallback
      (elementToggle) => { State.setState(elementToggle, 'show') }, // scrolledUpCallback
    )
  }
  static customLogos(slug) {
    // change the logo of the header, if it is a services page or space page/post.
    const customLogoPages = ['space', 'events', 'visual']
    if(customLogoPages.indexOf(slug) !== -1) {
      var logoImg = new Image()
      logoImg.src = `${top.location.origin.toString()}/app/themes/anomalous/dist/img/png/${slug}-logo.png` 
      logoImg.setAttribute('class', `h2 ${slug}-logo`)
    } else if (slug.includes('_space')) {
      var logoImg = new Image()
      logoImg.src = `${top.location.origin.toString()}/app/themes/anomalous/dist/img/png/space-logo.png` 
      logoImg.setAttribute('class', `h2 space-logo`)
    } else {
      var logoImg = new Image()
      logoImg.src = `${top.location.origin.toString()}/app/themes/anomalous/dist/img/png/full-logo.png` 
      logoImg.setAttribute('class', `h2 ${slug}-logo`)
    }

    let headerLogo = document.querySelector('#headerLogo')
    headerLogo.innerHTML = ''
    headerLogo.appendChild(logoImg)
  } 

  static logo (device) {
    // remove below as per client request.
    // if (device == 'mobile') { 
    //   var logoImg = new Image()
    //   logoImg.src = `${top.location.origin.toString()}/app/themes/anomalous/dist/img/png/mobile-logo.png` 
    //   logoImg.setAttribute('class', `h2 mobile-logo`)
    //   let headerLogo = document.querySelector('#headerLogo')
    //   headerLogo.innerHTML = ''
    //   headerLogo.appendChild(logoImg)
    // }

    let homepageAnchor = document.querySelector("a[href^='#home']")
    let squaresEl = document.getElementById('squares')
    
    homepageAnchor.addEventListener('click', function(e) {
      e.preventDefault()
    
      if (document.location.pathname !== '/') {
        State.toggleState(squaresEl, 'show', 'slideAway')
      }
    })  
  }
}