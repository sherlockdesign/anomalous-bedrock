import State from "../../../js/state";
import Mobile from 'is-mobile'

class Gallery {

  static gallery () {
    const gallery = document.getElementById('gallery');

    /**
     * There should always be a check to see if the element exist before
     * doing anything! You CANNOT asssume that the element will always be
     * there.
     */

    if(gallery !== null) {
      console.log('ANOMALOUS: Initiate the gallery.');
      
      [].forEach.call(gallery.querySelectorAll('a'), function(el) {
        el.addEventListener('click', (e) => {
          e.preventDefault()

          if (!Mobile()) {
            let anchor = e.currentTarget;
            // let galleryItem = anchor.querySelector('.gallery-item')
        
            if (anchor.getAttribute('data-state') !== 'active') {
              [].forEach.call(gallery.querySelectorAll('.gallery-link'), function(el) {
                State.setState(el, '')
              })    
            }
        
            State.toggleState(anchor, 'active', '')  
          }

        })
      })
    }
  }
}

export default Gallery