
import 'owl.carousel'
import observeMutations from './../../../js/observeMutations'

class Carousel {
  /**
   * Simply set the config for the class
   */
  constructor () {
    this.config = {
      nav: false,
      // navClass: ['owl-prev ml4 f3 outline-0 grow', 'owl-next mr4 f3 outline-0 grow'],
      dots: true,
      // dotsClass: 'owl-dots mw8 center bg-white-40 mt3 flex w-75',
      dotsClass: 'ml3 mr-auto owl-dots center bg-white-40 mt3 flex w-50',
      loop: true,
      items: 1,
      autoplay: true,
      lazyLoad: true,
      pullDrag: false,
      autoplayTimeout: 5000,
      animateOut: 'fadeOut',
      responsiveRefreshRate: 0,

      // Various events for triggering the centering of images
      onInitialized: (event) => {
        console.debug('____onInitialized')
        this.centerSlideImages(event)
      },
      onChanged: (event) => {
        console.debug('_____onChanged')
        this.centerSlideImages(event)
      },
      onResized: (event) => {
        console.debug('_____onResized')
        this.centerSlideImages(event)
      },
    }

    this.carouselElement = $('.background-carousel') 
    
    if(this.carouselElement.length !== 0) {
      this.carousel = this.carouselElement.owlCarousel(this.config)
      console.debug('Background Carousel: initiated')
    }
  }

  destroy () {
    if (typeof this.carousel !== 'undefined') {
      this.carousel.trigger('destroy.owl.carousel')
      console.debug('Background Carousel: destroyed')
    } else {
      return false
    }
  }
  
  centerSlideImages (event) {
    console.debug('Center the background carousel images.')

    let $slide = $(event.target),
    $conts = $slide.find('.owl-item')
    
    $conts.each(function() {
      let owlItem = $(this),
      owlItemWidth = owlItem.width(),
      owlItemHeight = owlItem.height(),
      img = owlItem.children('img'),
      imgWidth = img.width(),
      imgHeight = img.height(),
      conRatio = owlItemWidth / owlItemHeight,
      imgRatio = imgWidth / imgHeight,
      mode = (conRatio > imgRatio) ? 'portrait' : 'landscape'

      // console.debug(owlItem)
      // console.debug(`Container width: ${owlItemWidth}`)
      // console.debug(`Container height: ${owlItemHeight}`)
      // console.debug(img)
      // console.debug(`Image width: ${imgWidth}`)
      // console.debug(`Image height: ${imgHeight}`)
      // console.debug(`Container ratio: ${conRatio}`)
      // console.debug(`Image ratio: ${imgRatio}`)

      if( mode === 'portrait') {
        owlItem.removeClass('landscape')
        owlItem.addClass('portrait')
        img.css({
          marginTop: (owlItemHeight - imgHeight) / 2,
          marginLeft: 0,
        })
      } else {
        owlItem.removeClass('portrait')
        owlItem.addClass('landscape')
        // console.debug(`margin-left = ${owlItemWidth} - ${imgWidth} / 2 =  ${(owlItemWidth - imgWidth) / 2}`)
        img.css({
          marginTop: 0,
          marginLeft: (owlItemWidth - imgWidth) / 2,
        })
      }
    })
  }

  mutations () {
    const observeDOM = new observeMutations()
    observeDOM.config.subtree = false
    observeDOM.callback = function(mutationsList, observer) { 
      
      for(var mutation of mutationsList) {
    
        let addedNodes = mutation.addedNodes
    
        if(addedNodes.length > 0) {
      
          for (var key in addedNodes ) {
    
            if (addedNodes[key].id == null) {          
              if (this.carousel !== null) {
                console.log(this.carousel('destroy'))
              }
            } else {
              if (this.carousel !== null) {
                this.carousel('destroy')
              }
              if (addedNodes[key].id === 'servicePage' || addedNodes[key].id === 'spacePage') {
                this.carousel = $('.background-carousel').owlCarousel(this.config)
              }
            }
          }
        } 
      }
    }
    observeDOM.observe()
  }
}

export default Carousel
