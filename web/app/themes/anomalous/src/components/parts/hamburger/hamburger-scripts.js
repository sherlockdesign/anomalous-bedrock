
import State from '../../../js/state.js'

class Hamburger {

  constructor () {
    this.primaryMenuOpen = document.getElementById('primaryMenuOpen')
    this.primaryMenuClose = document.getElementById('primaryMenuClose')
    this.mainEl = document.querySelector('main')
    this.squares = document.getElementById('squares')
    this.overlay = document.querySelector('.menu-overlay')
  }

  init () {
    if (this.primaryMenuOpen !== null && this.primaryMenuClose !== null) {
      this.primaryMenuOpen.addEventListener ('click', (e) =>  this.openMenu(e))
      this.primaryMenuClose.addEventListener ('click', (e) =>  this.closeMenu(e)) 
      this.overlay.addEventListener ('click', (e) =>  this.closeMenu(e)) 
    }
  }

  openMenu () {
    State.setState(document.getElementById('primarySiteMenuModal'), 'show')
    if (this.squares !== null) { 
      if (this.squares.getAttribute('data-state') !== 'hide' 
      & this.squares.getAttribute('data-state') !== 'hidden'
      & this.squares.getAttribute('data-state') !== 'slideAway') {
        State.setState(this.squares, 'menu-open')
      }
    }
    if (this.mainEl.getAttribute('data-state') !== 'hide') {
      State.setState(this.mainEl, 'menushow')
    }
    State.setState(this.overlay, 'show')
  }

  closeMenu () {
    State.setState(document.getElementById('primarySiteMenuModal'), 'hide')
    if (this.squares !== null) {
      if (this.squares.getAttribute('data-state') !== 'hide' 
      & this.squares.getAttribute('data-state') !== 'hidden'
      & this.squares.getAttribute('data-state') !== 'slideAway') {
        State.setState(this.squares, 'visible')
      }
    }
    if (this.mainEl.getAttribute('data-state') !== 'hide') {
      State.setState(this.mainEl, 'menuhide')
    }
    State.setState(this.overlay, 'hide')
  }
}

export default Hamburger