
import observeMutations from './../../../js/observeMutations'
import State from './../../../js/state'

class SpacePage {

  constructor (device) {
    this.toggleButtons = document.querySelectorAll('.space-toggle-button')  
    this.applySpaceToggleButtonEvent()
    this.mutations()
    // if device is mobile, do not load the iframe.
    if(device !== true && typeof device !== 'undefined') {
      // add '#' to the href
      let iframe = document.getElementById('3dExperience')
      if(iframe !== null ) {
        iframe.src = iframe.getAttribute('data-src')
      }
    }
  }

  applySpaceToggleButtonEvent () {
    console.debug('applySpaceToggleButtonEvent')
    console.debug(this.toggleButtons)
    console.debug(this.toggleButtons !== null && typeof this.toggleButtons !== 'undefined')

    if (this.toggleButtons !== null && typeof this.toggleButtons !== 'undefined') {
      console.debug('Toggle buttons initiated');
      [].forEach.call(document.querySelectorAll('.space-toggle-button'), function(el) { 
  
        el.addEventListener('click', function(e) {
          e.preventDefault()
      
          let element = e.currentTarget
          let activeButton = document.querySelector('.space-toggle-button[data-state=active]')
          let activePreview = document.querySelector('.space-preview[data-state=active]')
          let target = element.getAttribute('data-target');
          let spacePreviewNode = document.querySelector(`#${target}`)
      
          if (element.getAttribute('data-state') === 'active') {
            return false
          }
      
          State.setState(element, 'active')
          State.setState(activeButton, 'inactive')
          State.setState(activePreview, 'inactive');
          State.setState(spacePreviewNode, 'active');
  
          // add the animation state for the currently active  preview
          let slideInImage =  spacePreviewNode.querySelector('.slide-in-image'),
          textReveal = spacePreviewNode.querySelector('.text-reveal')
          
          if (slideInImage !== null) {
            State.setState(slideInImage, 'show') 
          }
          if (textReveal !== null) {
            State.setState(textReveal, 'show') 
          }
  
        })
      })
    }
  }

  mutations () {
    // apply to future mutations of the DOM
    const observeDOM = new observeMutations()
    observeDOM.config.subtree = false
    observeDOM.callback = function(mutationsList, observer) { 
      
      for(var mutation of mutationsList) {

        let addedNodes = mutation.addedNodes

        if(addedNodes.length > 0) {
      
          for (var key in addedNodes ) {

            if (addedNodes[key].id == null) {
      
              // return false;
              
            } else {
              if (addedNodes[key].id === 'spacePage') {
                let SpacePage = new SpacePage()
                SpacePage.applySpaceToggleButtonEvent()
              }
            }
          }
        } 
      }
    }
    observeDOM.observe(this.device)
  }

}

export default SpacePage