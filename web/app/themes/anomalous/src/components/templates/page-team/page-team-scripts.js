import State from "../../../js/state"
import Ajax from '../../../js/Ajax'
import Mobile from 'is-mobile'

class Team {

  static gallery () {
    const gallery = document.querySelector('.team-members-gallery');
  
    /**
     * There should always be a check to see if the element exist before
     * doing anything! You CANNOT asssume that the element will always be
     * there.
     */
    
    if (gallery !== null) {
      
      [].forEach.call(document.querySelectorAll('a.team-gallery-link'), function(el) {
        // prevent the default click action (href)
        el.addEventListener('click', (e) => {
          e.preventDefault()
        })

        el.addEventListener('mouseover', (e) => {
          e.preventDefault()

          let anchor = e.currentTarget
          let teamContent = document.getElementById('teamContent')

          if (!Mobile()) {
            if (anchor.executing) {
              return
            }

            let target = anchor.getAttribute('href'),
            targetEl = document.querySelector(target)

            if(teamContent === null) {
              Team.getContent(anchor)

              if(anchor.executing) {
                let loaded = setInterval(() => {
                  if(!anchor.executing) {
                    clearInterval(loaded)
                    targetEl = document.querySelector(target)
                    if ( targetEl !== null) {
                      if (targetEl.getAttribute('data-state') !== 'active') {
                        [].forEach.call(document.querySelectorAll('.gallery-item-target'), function(el) {
                          State.setState(el, '')
                        })    
                      }
                      State.setState(targetEl, 'active')
                    }
                  }
                }, 50)
              }
            }
      
            if ( targetEl !== null) {
              if (targetEl.getAttribute('data-state') !== 'active') {
                [].forEach.call(document.querySelectorAll('.gallery-item-target'), function(el) {
                  State.setState(el, '')
                })    
              }
              State.setState(targetEl, 'active')
            }
          }
        })
        el.addEventListener('mouseout', (e) => {
          e.preventDefault()

          if (!Mobile()) {
            let anchor = e.currentTarget
      
            let target = anchor.getAttribute('href'),
            targetEl = document.querySelector(target)
      
            if (targetEl.getAttribute('data-state') !== 'active') {
              [].forEach.call(document.querySelectorAll('.gallery-item-target'), function(el) {
                State.setState(el, '')
              })    
            }
            State.setState(targetEl, '')
          }
        })
      })
      console.log('ANOMALOUS: Initiated the team members gallery.');
    }  
  }

  static getContent (anchor) {
    let content = document.getElementById('teamContent')

    // Modal not already in the DOM
    if (content === null) { 
      
      // creat the JSON obj
      var jsonString = {
        action: 'get_component',
        id: '48',
        template: 'templates/page-team/page-team-content',
        state: 'show',
        modelName: 'Team',
      }

      anchor.executing = true;

      Ajax.put(window.ajax_url, jsonString, function (response) {
        document.querySelector('header').insertAdjacentHTML('afterend', response);
        anchor.executing = false;
      })

    // is already in the DON
    } else {
      // State.setState(creditsModal, 'show')
    }
  }
}

export default Team


  