import Parallax from 'scroll-parallax'

import './style-guide-scripts'
import './style-guide-style.scss'

import './../../parts/hamburger/index'
import './../../parts/menu-modal/index'
import './../../parts/map/index'
import './../../parts/contact-modal/index'

import './../../parts/hero-right/index'
import './../../parts/point-contact/index'
import './../../parts/clients-carousel/index'
import './../../parts/video/index'

// var p = new Parallax('.parallax').init()
