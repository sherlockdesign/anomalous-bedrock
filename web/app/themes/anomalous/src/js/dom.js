
class DOMUtility {

  static ready(callback) {
    if('querySelector' in document
    && 'localStorage' in window
    && 'addEventListener' in window) {
      if (/comp|inter|loaded/.test(document.readyState)) {    
        callback()
      } else {
        document.addEventListener('DOMContentLoaded', function () {
          callback()
        })
      }
    }
  }

  static calculateRatio (w, h) {  
    const output = ((Math.round(h) / Math.round(w)) * 100).toFixed(2);
    if (output > 0) {
      return output.replace(".00", "") + "%"
    } 
    console.error(`w: ${w} h: ${h} output: ${output}`)
    return false
  }
  
  static setIframeVideoAspectRatioPad () {
    let iframeVideos = document.querySelectorAll("iframe[src*='//player.vimeo.com'], iframe[src*='//www.youtube.com/embed']")
  
    for (const iframe of iframeVideos) {
      let height = iframe.offsetHeight
      let width = iframe.offsetWidth
      let iframeRatioPadding = this.calculateRatio(width, height)
      let iframeParent = iframe.parentNode
  
      // remove iframe set width and height
      iframe.removeAttribute('width')
      iframe.removeAttribute('height')
  
      iframeParent.style.paddingBottom = iframeRatioPadding
      iframe.classList.add('aspect-ratio--object')
      
    }
  }

}

export default DOMUtility