import State from "./state";


class Scroll {

  static scrollElementToggle(
    elementToggle = null,
    elementArea = null,
    scrolledToTopCallback = null,
    scrolledDownCallback = null,
    scrolledPastElemCallback = null,
    scrolledUpAboveElemCallback = null,
    scrolledUpCallback = null
  ) {
  
    // We need to position the header above the window before we make it fixed so that
    // it can transition down
  
    let didScroll
    let lastScrollTop = 0
    const delta = 5
    var articleHeight = 0
    if(elementArea !== null) {
      articleHeight = document.querySelector(elementArea).offsetHeight 
    }
  
    window.addEventListener("scroll",
      (event) => { didScroll = true }
    )
  
    setInterval(function() {
     if (didScroll) {
        hasScrolled()
        didScroll = false
     }
    }, 250)
  
    var hasScrolled = () => {
      let st = window.pageYOffset
      let header = document.querySelector('header')
      let body = document.querySelector('body')
  
      // Make surce they scroll more than delta
      if(Math.abs(lastScrollTop - st) <= delta)
        return
  
      // Scrolled to the top
      if(st == 0) {    
       if (scrolledToTopCallback !== null) {
         scrolledToTopCallback(elementToggle)
       }
  
      // Scroll Down. So hide for better UX (reading)
      } else if (st > lastScrollTop && st > 0) {    
        if (scrolledDownCallback !== null) {
          scrolledDownCallback(elementToggle)
        }
  
        // Have we scrolled past the ""?
        if(st > articleHeight) {      
          if (scrolledPastElemCallback !== null) {
            scrolledPastElemCallback(elementToggle)
          }
        }
  
      // Scroll Up
      } else {    
  
        // If we're above the Elem Passed
        if(st < articleHeight) {      
          if (scrolledUpAboveElemCallback !== null) {
            scrolledUpAboveElemCallback(elementToggle)
          }
        } else {
          if (scrolledUpCallback !== null) {
            scrolledUpCallback(elementToggle)
          }
        }
      }
      lastScrollTop = st
    }
  }

  /**
   * 
   * @param {*} el 
   * @param {*} partially 
   */
  static isElementInViewPort (el, partially) {

    if(typeof(partially) === 'undefined') {
      var partially = false;
    }
  
    if(typeof jQuery === "function" && el instanceof jQuery) {
      el = el[0];
    }
  
    var rect = el.getBoundingClientRect();
    var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
    var windowWidth = (window.innerWidth || document.documentElement.clientWidth);
  
    // Partially in view
    if(partially) {
      var vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
      var horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);
      return (vertInView && horInView);
    }
  
    // Completely in view
    else {
      return ((rect.left >= 0) && (rect.top >= 0) && ((rect.left + rect.width) <= windowWidth) && ((rect.top + rect.height) <= windowHeight));
    }
  }

  ScrollEventsCallback() {
    console.log('empty')
  }

  scrollEvents () {
    window.addEventListener("scroll", (event) => {
      
      this.ScrollEventsCallback()
      
    })
  }
  
}

export default Scroll