
class ProgressiveEnhancement {
  
  constructor () {
    this.svgSupport = !!document.createElementNS &&
    !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect;
  }
  
  /**
   * 
   * @param {*} icon 
   */
  processIcon (icon) {
    // first check that the icons exists in the DOM
    let className = `.${icon.class}`
    let iconElements = document.querySelectorAll(className);  
    if (iconElements.length > 0) {
      // loop through 1 or more nodes
      for (var element of iconElements) {
        if (icon.svg) {
          let svg = new DOMParser().parseFromString(icon.svg, 'text/xml').children[0]
          if (icon.classNames) {
            svg.classList.add(...icon.classNames)
          }
          svg.setAttribute('fill', 'currentColor')
          element.replaceWith(svg)
        }
      }
    } else {
      return false;
    }
  }
  
  /**
   * 
   * @param {*} icons 
   */
  processIcons (icons) {
    if (this.svgSupport) {
      for (const key of Object.keys(icons)) {
        this.processIcon(icons[key])
      }
      console.debug(`Icons optimized to SVG`)
    } else {
      console.warn(`SVG not supported`)
    }
  } 

}

export default ProgressiveEnhancement