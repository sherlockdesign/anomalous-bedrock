class observeMutations {
  constructor () {
    // Select the node that will be observed for mutations
    this.targetNode = document.querySelector('main')

    // Options for the observer (which mutations to observe)
    this.config = { 
      attributes: true, 
      childList: true, 
      subtree: true
    }
  }

  // Callback function to execute when mutations are observed
  callback (mutationsList, observer) {
    console.log(mutationsList)
  }

  observe () {
    if (this.targetNode !== null) {
      // Create an observer instance linked to the callback function
      var observer = new MutationObserver(this.callback)

      // console.log( this.targetNode )

      // Start observing the target node for configured mutations
      observer.observe(this.targetNode, this.config)
    }
  }
  stopObserve () {
    observer.disconnect()
  }
}

export default observeMutations