import State from './state'

class Ajax {

    /**
     * 
     * @param {string} url 
     * @param {json obj} json 
     * @param {function} success 
     * @param {function} fail 
     */
   static put (url, json, success, fail) {
    var xhr = new XMLHttpRequest(),
    str = Object.keys(json).map(function(key) {
        return encodeURIComponent(key) + '=' + encodeURIComponent(json[key])
    }).join('&')

    xhr.open('PUT', url + '?' + str )
    xhr.setRequestHeader("Content-Type", "application/json")

    if( json != '' ) {
        var json_string = JSON.stringify( json )
        xhr.send(json_string)
    }

    xhr.onload = () => {
      if (xhr.status === 200) {
        if( typeof success === 'function' ) {
          success(xhr.responseText)
        } else {
          return xhr.responseText
        }
      } else if (xhr.status !== 200) {
        if( typeof fail === 'function' ) {
          fail()
        } else {
          alert('Request failed.  Returned status of ' + xhr.status)
        }
      }
    }
    return xhr
  }

  /**
  *
  * @desc callback placeholder for the internal links function
  */
  static internalLinkBefore () {
    return false
  }

  /**
   *
   * @desc Add the event listener to every internal link
   * 
   */
  static internalLinks () {
    var site_url = top.location.host.toString()
    var links = document.querySelectorAll("a");

    [].forEach.call(links, function(el) {
      var href = el.getAttribute('href')

      // href must be defined, match our site url and not be a email link.  
      if( href !== null && href.match(site_url) 
      && !href.match('mailto:') && !href.match('#home') 
      && !href.match('.pdf') && !href.match('.doc') 
      && !href.match('.jpg') && !href.match('.jpeg') ){
        
        // if link doesn't already contain the classname
        if ( !el.classList.contains('internal_link') && !el.classList.contains('no_ajax')  ) {
          el.classList.add('internal_link')

          // add click event
          el.addEventListener('click', function(e) {
            e.preventDefault()

            // var target_url = el.getAttribute('href')
            let current_url = window.location,
            menuItemFromHref = document.querySelector(`#primarySiteMenuModal nav a[href="${href}"]`);

            // remove previous active page class
            [].forEach.call(document.querySelectorAll('.menu-item.current_page_item'), function(el) {
              el.classList.remove('current_page_item')
            })

            if (e.currentTarget.classList.contains('menu-item')) {
              e.currentTarget.classList.add('current_page_item')
            } else if (menuItemFromHref !== null && typeof menuItemFromHref !== 'undefined') {
              menuItemFromHref.classList.add('current_page_item')
            }

            let squaresEl = document.getElementById('squares')

            if (href != current_url) {
              if( Ajax.getPage(href) != false ) {
                history.pushState(null, null, href)
              }
            } else if (squaresEl.getAttribute('data-state') === 'show') {
              State.setState(squaresEl, 'slideAway')
            }
            e.stopPropagation()
          })

        }
      }
    })
  }

  /**
   * 
   * @param {string} url 
   */
  static getPage (url) {

    Ajax.internalLinkBefore()

    const slug = url.match(/[^/]*(?=(\/)?$)/)[0]

    var json_string = JSON.stringify({
      ajax: true,
      slug
    })
    var xhr = new XMLHttpRequest()

    xhr.open('POST', url )
    xhr.setRequestHeader("Content-Type", "application/json")
    xhr.send(json_string)
    xhr.onload = () => {
      if (xhr.status === 200) {
        var response = xhr.responseText
        Ajax.getPageCallback(response, slug)
      } else if (xhr.status !== 200) {
        return false
      }
    }
  }

  /**
  * @module links
  *
  * @desc return the result of a URL
  *
  * @param url (String)
  */
  static links (documentMainStr, before, after) {
    var documentMain = document.querySelector(documentMainStr),
    site_url = 'http://' + top.location.host.toString(),
    internal_links = document.querySelectorAll("a[href^='" + site_url +"']")

    // Add call to the links so that we only target internal links.
    internal_links.classList.add('internal_links');

    [].forEach.call(internal_links, function(el) {
      el.classList.add('internal_link')
      el.addEventListener('click', function(event) {
        event.preventDefault()

        // Callback before
        before()

        var url = el.getAttribute('href'),
        json_string = JSON.stringify({ ajax: true }),
        xhr = new XMLHttpRequest()

        // Open the url from the link
        // Send ajax true property so the template does not return the
        // whole HTML.
        xhr.open('PUT', url )
        xhr.setRequestHeader("Content-Type", "application/json")
        xhr.send(json_string)
        xhr.onload = () => {
          if (xhr.status === 200) {
            var response = xhr.responseText
            documentMain.innerHTML = response
            after()
          }
          else if (xhr.status !== 200){
            alert('Request failed.  Returned status of ' + xhr.status)
          }
        }
      })
    })
  }
}

export default Ajax