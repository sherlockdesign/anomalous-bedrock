
import State from './state'
import observeMutations from './observeMutations'
import Parallax from 'scroll-parallax'

class progressiveImages {

  static progressivelyLoadImage (placeholder, device) {
    let small = placeholder.querySelector('.progressive-image-tiny')

    /**
     * 1: load small image and show it
     */
    if (small) {
      var img = new Image(),
      
      imgSrc = small.src

      // if data-src is not detectable through .src
      if(imgSrc == null) {
        imgSrc = small.getAttribute('data-src')
      }

      img.src = imgSrc
      img.onload = function () {
       small.classList.add('loaded')
       State.setState(small, 'loaded')
      }

      var ratio = placeholder.getAttribute('data-ratio')
      
      
      /**
       * 2: load large image
       */
      var imgLarge = new Image(),
      imgBackground = document.createElement('div')
       
      if (device == 'mobile') {
        switch (ratio) {
          case '1x1':
            var imgSrc = placeholder.getAttribute('data-src-mobile-1x1')
            
            break;
          case '3x2':
            var imgSrc = placeholder.getAttribute('data-src-mobile')
            break;
          default:
            var imgSrc = placeholder.getAttribute('data-src-mobile')
            break;
        }
      } else {
        switch (ratio) {
          case '1x1':
            var imgSrc = placeholder.getAttribute('data-src-desktop-1x1')
            break;
          case '3x2':
            var imgSrc = placeholder.getAttribute('data-src-desktop')
            break;
          default:
            var imgSrc = placeholder.getAttribute('data-src-desktop')
            break;
        }
      }

      if(imgSrc !== null) {
        imgLarge.src =  imgSrc
      }
      imgLarge.setAttribute('class', 'absolute absolute--fill parallax parallax-2')
      
      // imgLarge.srcset = placeholder.getAttribute('data-srcset-large')
      // imgLarge.sizes = placeholder.getAttribute('data-sizes-large')

      imgLarge.onload = function () {

        // if is a background image (IE not a parallax image)
        if(placeholder.classList.contains('progressive-image-background')) {
          if(placeholder.classList.contains('progressive-image-background-portrait')) {
            imgBackground.setAttribute('class', 'h-100 absolute absolute--fill bg-top cover')
          } else {
            imgBackground.setAttribute('class', 'h-100 absolute absolute--fill bg-center cover')
          }
          imgBackground.style.backgroundImage = `url(${imgSrc})`
          
          imgBackground.setAttribute('data-state', 'loaded')
          placeholder.appendChild(imgBackground)
        
        // Parallax requires an <img>
        } else {
          placeholder.appendChild(imgLarge)
        }
      }
      State.setState(placeholder, 'loaded')
    }
  }

  static progressivelyLoadImages (device) {
    // apply to loaded DOM
    [].forEach.call(document.querySelectorAll('.progressive-image-placeholder'), function(el) { 
      progressiveImages.progressivelyLoadImage(el, device)
    })

    // apply to future mutations of the DOM
    const newImages = new observeMutations()
    newImages.callback = function(mutationsList, observer) {

      for(var mutation of mutationsList) {

        let addedNodes = mutation.addedNodes

        if(addedNodes.length > 0) {

          for (var key in addedNodes ) {

            if (addedNodes[key].classList == null) {

              return false;
              
            } else {

              if (addedNodes[key].classList.contains('progressive-image-placeholder')) {
                progressivelyLoadImage(addedNodes[key])

              } else if (addedNodes[key].classList.contains('parallax')) {
                console.warn('parallax img added to the DOM')

                new Parallax('.parallax').on('element:loaded', () => {
                  console.warn('Yo, bro! The parallax is loaded!')
                }).init() 

                new Parallax('.parallax-2').on('element:loaded', () => {
                  console.warn('Yo, bro! The second paralax is loaded!')
                }).init() 
              }
            }
          }
        }
      }
    }
    newImages.observe()
  }
}

export default progressiveImages