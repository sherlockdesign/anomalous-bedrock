
import fullLogoSvg from './../svg/full-logo.svg'
import visualLogoSvg from './../svg/visuals-logo.svg'
import eventsLogoSvg from './../svg/events-logo.svg'
import spaceLogoSvg from './../svg/space-logo.svg'

import mobileLogoSvg from './../svg/mobile-logo.svg'

import facebookLogoSvg from './../svg/facebook-logo.svg'
import twitterLogoSvg from './../svg/twitter-logo.svg'
import instagramLogoSvg from './../svg/instagram-logo.svg'
import linkedinLogoSvg from './../svg/linkedin-logo.svg'
import emailIconSvg from './../svg/email-logo.svg'
import vimeoLogoSvg from './../svg/vimeo-logo.svg'
import youtubeLogoSvg from './../svg/youtube-logo.svg'
import soundcloudLogoSvg from './../svg/soundcloud-logo.svg'
import downArrowSvg from './../svg/down-arrow.svg'
import bigQuoteSvg from './../svg/big-quote.svg'
import playIconSvg from '../svg/play-icon.svg'

export const fullLogo = {
  class: 'full-logo',
  classNames: ['h2'],
  svg: fullLogoSvg
}

export const mobileLogo = {
  class: 'mobile-logo',
  classNames: ['h2'],
  svg: mobileLogoSvg
}

export const visualLogo = {
  class: 'visuals-logo',
  classNames: ['h2'],
  svg: visualLogoSvg
}

export const eventsLogo = {
  class: 'events-logo',
  classNames: ['h2'],
  svg: eventsLogoSvg
}

export const spaceLogo = {
  class: 'space-logo',
  classNames: ['h2'],
  svg: spaceLogoSvg
}

export const facebookLogo = {
  class: 'facebook-logo',
  classNames: ['h1', 'mr2', 'center', 'db'],
  svg: facebookLogoSvg,
  colors: ['persian-green', 'white']
}

export const twitterLogo = {
  class: 'twitter-logo',
  classNames: ['h1', 'mr2', 'center', 'db'],
  svg: twitterLogoSvg,
  colors: ['persian-green', 'white']
}

export const instagramLogo = {
  class: 'instagram-logo',
  classNames: ['h1', 'mr2', 'center', 'db'],
  svg: instagramLogoSvg,
  colors: ['persian-green', 'white']
}

export const linkedinLogo = {
  class: 'linkedin-logo',
  classNames: ['h1', 'mr2', 'center', 'db'],
  svg: linkedinLogoSvg,
  colors: ['persian-green', 'white']
}

export const emailIcon = {
  class: 'email-logo',
  classNames: ['h1', 'mr2', 'center', 'db'],
  svg: emailIconSvg,
  colors: ['persian-green', 'white']
}

export const vimeoLogo = {
  class: 'vimeo-logo',
  classNames: ['h1', 'mr2', 'center', 'db'],
  svg: vimeoLogoSvg,
  colors: ['persian-green', 'white']
}

export const youtubeLogo = {
  class: 'youtube-logo',
  classNames: ['h1', 'mr2', 'center', 'db'],
  svg: youtubeLogoSvg,
  colors: ['persian-green', 'white']
}

export const soundcloudLogo = {
  class: 'soundcloud-logo',
  classNames: ['h1', 'mr2', 'center', 'db'],
  svg: soundcloudLogoSvg,
  colors: ['persian-green', 'white']
}

export const playIcon = {
  class: 'play-icon',
  // classNames: ['h1', 'mr2', 'center', 'db'],
  svg: playIconSvg,
  classNames: ['h3', 'db', 'center'],
}

export const bigQuote = {
  class: 'big-quote',
  // classNames: ['h1', 'mr2', 'center', 'db'],
  svg: bigQuoteSvg,
  classNames: ['mb4', 'db', 'center', 'h2'],
}

export const downArrow = {
  class: 'down-arrow-icon',
  // classNames: ['h1', 'mr2', 'center', 'db'],
  svg: downArrowSvg,
  classNames: ['db', 'center', 'w2', 'h3'],
}