import Mobile from 'is-mobile'

import State from './js/state.js'
import * as icons from './js/assets.js'
import Ajax from './js/Ajax'
import DOMUtility from './js/dom'
import ProgressiveEnhancement from './js/ProgressiveEnhancement'
import progressiveImages from './js/progressiveImages'

import './sass/styles.scss'

// Component Style
import './components/parts/header/index'
import './components/parts/squares/index'
import './components/parts/hamburger/index'
import './components/parts/menu-modal/index'
import './components/parts/contact-modal/index'
import './components/parts/progressive-image/index'
import './components/parts/clients-carousel/index'
import './components/parts/hero-right/index'
import './components/parts/video/index'
import './components/parts/categories-menu/index'
import './components/parts/gallery/index'
import './components/parts/credits-modal/index'
import './components/parts/progressive-image-parallax/index'
import './components/parts/background-video/index'
import './components/parts/carousel/index'
import './components/parts/point-contact/index'
import './components/parts/social-share-icons/index'
import './components/parts/loadMore/index'
import './components/templates/single-work-1/index'
import './components/templates/single-work-3/index'
import './components/templates/single-work-4/index'


// template styles
import './components/templates/app/index'
import './components/templates/page-team/index'
import './components/templates/single-work-2/index'
import './components/parts/services-content/index'

//Component Classes
import AnimatedElements from './components/parts/animated-elements/animated-elements-scripts';
import Carousel from './components/parts/carousel/carousel-scripts'
import Video from './components/parts/video/video-scripts'
import ClientsCarousel from './components/parts/clients-carousel/clients-carousel-scripts'
import ContactModal from './components/parts/contact-modal/contact-modal-scripts'
import Hamburger from  './components/parts/hamburger/hamburger-scripts'
import Header from './components/parts/header/header-scripts'
import LightboxGallery from './components/parts/lightbox-gallery/lightbox-gallery-scripts'
import creditsModal from './components/parts/credits-modal/credits-modal-scripts'
import { loadMore, ObserveNewPosts } from './components/parts/loadMore/load-more-scripts'
import Team from './components/templates/page-team/page-team-scripts'
import SpacePage from './components/templates/page-space/page-space-scripts'
import Squares from './components/parts/squares/squares-scripts'


// has functionality 
import './components/templates/page-space/index'

// This CSS needs to override the default style in Magnific Popup (LightboxGallery)
import './components/parts/lightbox-gallery/index'

class Anomalous {

  constructor(device) {
    this.device = (device === true) ? 'mobile' : 'not mobile'
    this.mobile = device
    this.enhance = new ProgressiveEnhancement()
    this.squares = document.getElementById('squares')

    console.log(`This device is ${this.device}`)
  }

  /**
   * Initialize the app upon first load of the page
   */
  initialize () {
    // header toggle show/hide
    Header.scroll()

    // logo event listener
    Header.logo(this.device)
    console.log(Squares)
    if(this.device !== 'mobile') {
      Squares.loadGifs()
    }

    // hamburger event listeners
    this.hamburger = new Hamburger()
    this.hamburger.init()

    this.ContactModal = new ContactModal()
  }

  /**
   * Things to call specific to navigating the page
   */ 
  ajaxCallback(response, callback, slug) {
    // make sure that we always start at the top of the page.
    window.scrollTo(0, 0)

    if (slug !== 'team') {
      let teamContent = document.getElementById('teamContent')
      if(teamContent !== null) {
        teamContent.parentNode.removeChild(teamContent)
      }
    }

    // add the new content to the DOM
    let mainEl = document.querySelector('main')
    
    mainEl.innerHTML = response;

    //set page title 
    let pageTitle = document.getElementById('pageTitle')
    if(pageTitle !== null) {
      document.querySelector('title').innerHTML = pageTitle.getAttribute('data-title')
    }
    
    // mainEl.style.display = 'block'
    State.setState(mainEl, 'show')

    // update the squares fill url
    let squaresNodelist = document.querySelectorAll('.square')
    // loop through
    var i = 1;
    for (const square of squaresNodelist) {
      let imgLayers = square.querySelectorAll('.img-layer')
      for (const layer of imgLayers) {
        layer.setAttribute('style', `fill:url(${window.location.href}#p-img-${i})`)  
      }
      i++
    }

    callback()
  }

   // things to load on init and navigation
   start () {
    // make sure the page always begins at the top.
    window.scrollTo(0, 0)

    this.enhance.processIcons(icons)
    
    // exception for the background video because it is always to be loaded.
    let backgroundVideoEl = document.querySelector('#backgroundVideo')
    if (backgroundVideoEl !== null) {
      backgroundVideoEl.setAttribute('data-state', 'show')
    }

    this.animations = new AnimatedElements()
    setTimeout(() => {
      this.animations.onLoadAnimations()

      /// See 'animated-elements-scripts.js'
      this.animations.onScrollAnimations()
    }, 1000)

    // this.progressiveImages = new progressiveImages()
    // this.progressiveImages.progressivelyLoadImagesOnScroll(this.device)  

    progressiveImages.progressivelyLoadImages(this.device)  

    Ajax.internalLinks()

    if ('undefined' !== typeof this.ClientsCarousel) {
      // Destroy previous page carousel.
      this.ClientsCarousel.destroy()
      
      // Initialize new carousel.
      this.clientsCarousel = new ClientsCarousel()  
    } else {
      // Initialize new carousel.
      this.clientsCarousel = new ClientsCarousel()  
    }

    /**
     * strange fix. The Timeout doesn't need to wait.
     * An anonymous function does not do the same.
    */ 
    setTimeout(() => {
      
      // Is there a carousel that already exists?
      if ('undefined' !== typeof this.Carousel) {
        // Destroy previous page carousel.
        this.Carousel.destroy()
        
        // Initialize new carousel.
        this.Carousel = new Carousel()  
      } else {
      // Initialize new carousel.
        this.Carousel = new Carousel()  
      }
    }, 0)

    this.Video = new Video()
    
    this.SpacePage = new SpacePage(this.mobile)

    Team.gallery()

    LightboxGallery.init()

    creditsModal.init()

    loadMore.init()

    let posts = new ObserveNewPosts()
    posts.observe()
  }

  popState() {
    if (window.location.hash) {
      return false
    } else if (document.location.pathname == '/') {
      State.toggleState(this.squares, 'show', 'slideAway')
    } else {
      Ajax.getPage(document.location.href)
    } 
  }
}
let isMobile = Mobile()

let app = new Anomalous( isMobile )

DOMUtility.ready(() => {
  app.initialize()
  app.start()
})

window.onpopstate = (event) => {
  console.log(event)
  app.popState()
  app.start()
}

Ajax.internalLinkBefore = () => {
  // if the menu is open, close is
  if (document.getElementById('primarySiteMenuModal').getAttribute('data-state') === 'show') {
    app.hamburger.closeMenu()
  }

  // If the user navigated using the squares, hide them.
  const squaresEl = document.getElementById('squares')
  if (squaresEl !== null) {
    const squareState = squaresEl.getAttribute('data-state')
    if (squareState === 'show' || squareState === 'visible') {
      State.setState(squaresEl, 'hide')
    }
  }
}

Ajax.getPageCallback = (response, slug) => {
  
  app.ajaxCallback(response, () => { app.start() }, slug)

  // console.log(`readyState: ${/comp|inter|loaded/.test(document.readyState)}`)
}