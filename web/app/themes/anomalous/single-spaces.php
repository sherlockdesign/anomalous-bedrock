<?php
/**
* Template Name: Single Space
* Template Post Type: spaces
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

$space = new Controllers\SingleSpace();
$space->show();
